﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using mLender.Models;
using mLender.Service;
using WebMatrix.WebData;

namespace mLender.Controllers
{
    //[Authorize]

    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        string branch_name;

        JiokoeDbContext jiokoeDb;

        string AuditID = GetCurrentUserService.CurrentUser();

        private static string ConnectionString =
                           ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private static SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        public string Name;

        public AccountController()
        {
            jiokoeDb = new JiokoeDbContext();
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    //if logged in user is an agent,redirect accordingly

                    var user = await UserManager.FindAsync(model.UserName, model.Password);
                    var roles = await UserManager.GetRolesAsync(user.Id);
                    if (roles.Contains("Agent"))
                    {
                        
                        var agentIDNo= jiokoeDb.Users.Where(u => u.Id == user.Id).Select(u => u.IDNo).SingleOrDefault();
                        Session["agentIDNo"] = agentIDNo;

                        var usermail= jiokoeDb.Users.Where(u => u.Id == user.Id).Select(u => u.Email).SingleOrDefault();

                        userRepository userRepository = new userRepository();

                        var branch = userRepository.getBranch(usermail);

                        Session["userbranch"] = branch;

                        return RedirectToAction("index", "member", new { area = "" });
                        //return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        return RedirectToLocal(returnUrl);

                    }


                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AccessDeniedAuthorize(Roles = "Manager,Admin")]

        public ActionResult Register()
        {
            //do not show admin or manager role when user is registering
            ViewBag.Name = new SelectList(jiokoeDb.Roles.Where(u => !u.Name.Contains("Admin")/*||!u.Name.Contains("Manager")*/)
                                            .ToList(), "Name", "Name");

            //ViewBag.Name = new SelectList(SDLC.Roles.ToList(), "Name", "Name");
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]  
        //[AllowAnonymous]
        [Authorize]
        [AccessDeniedAuthorize(Roles = "Manager,Admin")]

        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser 
                { UserName = model.UserName, Email = model.Email,
                    FirstName = model.FirstName,
                    OtherName = model.OtherName,
                    IDNo = model.IDNo,
                    Telephone = model.Telephone,
                };

                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    try
                    {

                        //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                        await this.UserManager.AddToRoleAsync(user.Id, model.UserRoles);

                        ///if use is an agent...insert to the agents table
                        ///

                        //var agent = jiokoeDb.Roles.Where(u => u.Name.Contains("Agent")).FirstOrDefault();
                        using (sqlConnection)
                        {

                            sqlConnection.Open();


                        if (model.UserRoles.ToLower()=="Agent".ToLower())
                        {

                            var officer_id = "";
                            var first_name = "";
                            var other_name = "";
                            var telephone = "";
                            var email_address = "";
                            var id_no = "";
                            var audit_id = AuditID;
                            var audit_time = DateTime.Now;

                          
                                    var insertOfficer = sqlConnection.Execute("saveofficerdetails", new
                                    {
                                        officer_id,
                                        first_name = model.FirstName,
                                        other_name = model.OtherName,
                                        telephone = model.Telephone,
                                        email_address = model.Email,
                                        id_no = model.IDNo,
                                        audit_id,
                                        audit_time


                                    }, commandType: CommandType.StoredProcedure);

                                //do not show admin or manager role when user is registering
                                ViewBag.Name = new SelectList(jiokoeDb.Roles.Where(u => !u.Name.Contains("Admin")/*||!u.Name.Contains("Manager")*/)
                                                                .ToList(), "Name", "Name");

                                return RedirectToAction("Index", "companyofficer");

                            }
                            else
                            {
                                return RedirectToAction("Index", "Users");

                            }

                        }


                        //return RedirectToAction("Index", "Users");
                    }
                    catch (Exception ex)
                    {

                        throw new Exception(ex.Message);
                    }



                }

                ViewBag.Name = new SelectList(jiokoeDb.Roles.Where(u => !u.Name.Contains("Admin"))
                                          .ToList(), "Name", "Name");


                //ViewBag.Name = new SelectList(SDLC.Roles.ToList(), "Name", "Name");
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            var userMail = model.Email;
            string resetCode = Guid.NewGuid().ToString();
            var verifyUrl = "/Account/ResetPassword/" + resetCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);
            using (var context = new JiokoeDbContext())
            {
                var getUser = (from s in context.Users where s.Email == userMail select s).FirstOrDefault();
                if (getUser != null)
                {
                    getUser.ResetPasswordCode = resetCode;

                    //This line I have added here to avoid confirm password not match issue , as we had added a confirm password property 

                    context.Configuration.ValidateOnSaveEnabled = false;
                    context.SaveChanges();

                    var subject = "Password Reset Request";
                    var body = "Hi " + getUser.FirstName + ",  " +
                        "You recently requested to reset your password for your account. Click the link below to reset it. " +

                         "<a href='" + link + "'>"+
                         "If you did not request a password reset, please ignore this email or reply to let us know.Thank you";


                    EmailManager.SendEmail("Test@gmail.com",subject,body,getUser.Email, "test@acumenstaffing.co.ke", "123456789", "587", "mail.acumenstaffing.co.ke");

                    ViewBag.Message = "Reset password link has been sent to your email id.";
                }
                else
                {
                    ViewBag.Message = "User doesn't exists.";
                    return View();
                }
            }

            return View();
        }
       

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string id)
        {
            
                //Verify the reset password link
                //Find account associated with this link
                //redirect to reset password page
                if (string.IsNullOrWhiteSpace(id))
                {
                    return HttpNotFound();
                }

                using (var context = new JiokoeDbContext())
                {
                    var user = context.Users.Where(a => a.ResetPasswordCode == id).FirstOrDefault();
                    if (user != null)
                    {
                        ResetPasswordViewModel model = new ResetPasswordViewModel();
                        model.Code = id;
                        return View(model);
                    }
                    else
                    {
                        return HttpNotFound();
                    }
                }
            }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var message = "";
                if (ModelState.IsValid)
                {
                    using (var context = new JiokoeDbContext())
                    {
                        var user = context.Users.Where(a => a.ResetPasswordCode == model.Code).FirstOrDefault();
                        if (user != null)
                        {
                            //you can encrypt password here, we are not doing it
                            user.PasswordHash = model.Password;
                            user.PasswordHash = Crypto.HashPassword(model.Password);
                            //make resetpasswordcode empty string now
                            user.ResetPasswordCode = "";
                            //to avoid validation issues, disable it
                            context.Configuration.ValidateOnSaveEnabled = false;
                            context.SaveChanges();
                            message = "New password updated successfully";
                        }
                    }
                }
                else
                {
                    message = "Something invalid";
                }
                ViewBag.Message = message;
            }
            return View(model);

        }   

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff(string token)
        {
            //FormsAuthentication.SignOut();
            //Session.Abandon(); // it will clear the session at the end of request
            //return RedirectToAction("login","account");
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            return RedirectToAction("Index", "Home");

        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}