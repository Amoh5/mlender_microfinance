﻿using Dapper;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace mLender.Controllers
{
    [AccessDeniedAuthorize(Roles = "Manager,Admin")]

    public class CompanyBranchesController : Controller
    {


        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private CompanyBranches company = new CompanyBranches();
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();

        string branch_id = "";
        private CompanyOfficer companyOfficer = new CompanyOfficer();
        // GET: /CompanyBranches/
        [Authorize]

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();
                    List<CompanyBranches> company = sqlConnection.Query<CompanyBranches>
                                 ("get_all_active_companies", null, commandType: CommandType.StoredProcedure).ToList();
                    return View(company);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /CompanyBranches/Details/5
        [Authorize]

        public ActionResult Details(int id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var company_details = sqlConnection.Query<CompanyBranches>
                        ("get_company_edit", new { id }, commandType: CommandType.StoredProcedure).SingleOrDefault();


                    //get true or false value for check-off
                    var check_off = JiokoeDb.Database.SqlQuery<CompanyBranches>
                        ("select allow_check_off from branch where branch_id=" + id)
                        .Select(c => c.allow_check_off).SingleOrDefault();
                    ViewBag.check_off = check_off.ToString();
                    return View(company_details);
                }
            }
            catch (Exception ex)
            {

                TempData["Error"] = ex.Message;
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /CompanyBranches/Create
        [Authorize]

        public ActionResult Create()
        {

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    IEnumerable<CompanyOfficer> officers =
                                    sqlConnection.Query<CompanyOfficer>
                                 ("get_officer", commandType: CommandType.StoredProcedure);


                    ViewBag.officer = new SelectList(officers, "officer_id", "fullname");
                    //get employer debtor account

                    IEnumerable<Ledger> employer_ledger = sqlConnection.Query<Ledger>
                        ("get_account_receivables", null, commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.Employer_Ledger = new SelectList(employer_ledger, "code", "name");

                    return View(company);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /CompanyBranches/Create

        [HttpPost]
        [Authorize]

        public JsonResult Create(CompanyBranches branches,string branch_id)
        {
            try
            {
                SaccoDetailsRepository saccoDetails = new SaccoDetailsRepository();

                var add_branch = saccoDetails.Add(branches, branch_id);

                TempData["Success"] = "Details saved succesfully";


                return Json(TempData["Success"].ToString(),JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Details not saved! " + ex.Message;

                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);

            }
        }

        //
        // GET: /CompanyBranches/Edit/5
        [Authorize]
        [HttpGet]

        public ActionResult Edit(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //get officers to populate dropdwn
                    List<CompanyOfficer> officer = sqlConnection.Query<CompanyOfficer>
                                ("get_company_officer", null,
                                commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.officer = new SelectList(officer, "officer_id", "fullname"); ;

                    //populate the dropdown for ledgers
                    List<Ledger> employer_ledger = sqlConnection.Query<Ledger>
                       ("get_account_receivables", null, commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.Employer_Ledger = new SelectList(employer_ledger, "code", "Name");



                    var get_branch_edit = sqlConnection.Query<CompanyBranches>("get_company_edit", new { @id = id },
                        commandType: CommandType.StoredProcedure).SingleOrDefault();
                    if (get_branch_edit == null)
                    {
                        return HttpNotFound();
                    }
                    else
                    {
                        return View(get_branch_edit);

                    }
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        //
        // POST: /CompanyBranches/Edit/5

        [HttpPost]
        [Authorize]

        public ActionResult Edit(string id, CompanyBranches model)
        {
            try
            {
                SaccoDetailsRepository saccoDetails = new SaccoDetailsRepository();

                var edit_branch = saccoDetails.Edit(id, model);
                TempData["Success"] = "Changes saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                TempData["Error"] = "Changes not saved! " + ex.Message;

                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /CompanyBranches/Delete/5

        public ActionResult Delete(int id)
        {
            using (sqlConnection)
            {
                sqlConnection.Open();
                var company_delete = sqlConnection.Query<CompanyBranches>
                     ("get_company_edit", new { @id = id },
                     commandType: CommandType.StoredProcedure).SingleOrDefault();


                return View(company_delete);
            }
        }

        //
        // POST: /CompanyBranches/Delete/5

        [HttpPost]
        [Authorize]

        public JsonResult Delete(string id, CompanyBranches company)
        {
            try
            {
                using (sqlConnection)
                {
                    SaccoDetailsRepository saccoDetails = new SaccoDetailsRepository();
                    saccoDetails.Delete(id, company);

                    return Json(TempData["success"] = "Details deleted successfully! ", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(TempData["Error"] = "Details not deleted! " + ex.Message, JsonRequestBehavior.AllowGet);

            }
        }
    }
}
