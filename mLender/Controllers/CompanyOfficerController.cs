﻿using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [AccessDeniedAuthorize(Roles = "Manager,Admin")]

    public class CompanyOfficerController : Controller
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();
        private CompanyOfficer officer = new CompanyOfficer();

        private CompanyOfficerRepository officerRepository = new CompanyOfficerRepository();


        //
        // GET: /CompanyOfficer/
        [Authorize]

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    List<CompanyOfficer> officers =
                        sqlConnection.Query<CompanyOfficer>
                        ("select * from officer where Isactive=1", null, commandType: CommandType.Text).ToList();
                    return View(officers);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        //
        // GET: /CompanyOfficer/Details/5
        [Authorize]

        public ActionResult Details(int id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    officer = sqlConnection.Query<CompanyOfficer>
                        ("select * from officer where officer_id=" + id).SingleOrDefault();
                }
                return View(officer);
            }
            catch (Exception)
            {

                throw;
            }

        }

        //
        // GET: /CompanyOfficer/Create
        [Authorize]

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CompanyOfficer/Create

        [HttpPost]
        public JsonResult Create(CompanyOfficer companyOfficer,string id)
        {
            oErrorLog oError = new oErrorLog();

            try
            {
                var insert_details= officerRepository.AddOfficer(companyOfficer,id);
                TempData["Success"] = "Details saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oError.WriteErrorLog(ex.Message);
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /CompanyOfficer/Edit/5
        [Authorize]

        public ActionResult Edit(int id)
        {
            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();
                    var edit_officer = sqlConnection.Query<CompanyOfficer>
                        ("select * from officer where officer_id=" + id).FirstOrDefault();
                    if (edit_officer != null)
                    {
                        return View(edit_officer);

                    }
                    else
                    {
                        //var User_message = new HttpStatusCodeResult(404, "There is no record in database to edit");
                        var User_message = new HttpNotFoundResult("There is no record in database to edit");

                        return User_message;
                    }
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }

        }

        //
        // POST: /CompanyOfficer/Edit/5
        [Authorize]

        [HttpPost]
        public JsonResult  Edit(CompanyOfficer officer, string id)
        {
            try
            {
                return Create(officer, id);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Details not Updated! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);

            }
        }

        //
        // GET: /CompanyOfficer/Delete/5
        [Authorize]

        public ActionResult Delete(int id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    officer = sqlConnection.Query<CompanyOfficer>
                       ("select * from officer where isactive=1 and officer_id=" + id).SingleOrDefault();

                    return View(officer);

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
                //return RedirectToAction("Index");
            }


        }

        //
        // POST: /CompanyOfficer/Delete/5

        [HttpPost]
        [Authorize]

        public JsonResult Delete(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    var delete = officerRepository.DeleteOfficer(id);
                }

                TempData["message"] = "Details deleted! ";
                return Json(TempData["message"].ToString(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                TempData["Error"] = "Details not deleted! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);

            }
        }
    }
}
