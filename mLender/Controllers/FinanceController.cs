﻿using Dapper;
using mLender.Models;
using mLender.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [AccessDeniedAuthorize(Roles = "Accountant,Admin,Manager")]
    public class FinanceController : Controller
    {

        private static string ConnectionString =
                ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        oErrorLog oErrorLog = new oErrorLog();
        CultureInfo culture = new CultureInfo("en-GB");
        // GET: Finance

        [Authorize]
        [HttpGet]
        public ActionResult CashBook()
        {
            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();

                    //get ledger  accounts

                    IEnumerable<Ledger> ledgers =
                        sqlConnection.Query<Ledger>("[get_ALL_ledgers]", null, commandType: CommandType.StoredProcedure);

                    ViewBag.ledgers = new SelectList(ledgers, "code", "name");


                    IEnumerable<CompanyBranches> branch =
                        sqlConnection.Query<CompanyBranches>("get_all_active_companies", null, commandType: CommandType.StoredProcedure);

                    ViewBag.branch = new SelectList(branch, "branch_id", "branch_name");




                    return View();
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public JsonResult cashbook_transaction(string ledger,string startDate,string endDate, string branch_id)
        {

            try
            {

                TempData["ledger"] = ledger;
                TempData["startDate"] = startDate;
                TempData["endDate"] = endDate;
                TempData["branch"] = branch_id;


                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //PASS DATE AS STRING AND CONVER THEM IN SQL SP===SOLVES jsON DATE FORMAT ISSUE

                 var cashbook_transactions= sqlConnection.Query("generateCashbookTransactions",
                        new
                        {
                            ledger,
                            startDate=Convert.ToDateTime(startDate).ToShortDateString(),
                            endDate = Convert.ToDateTime(endDate).ToShortDateString(),

                            branch_id
                        },commandType:CommandType.StoredProcedure);

                    return Json(cashbook_transactions, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json("Error "+ex.Message, JsonRequestBehavior.AllowGet);

            }
        }
        [Authorize]
        public ActionResult TrialBalance()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();



                    IEnumerable<CompanyBranches> branch =
                        sqlConnection.Query<CompanyBranches>("get_all_active_companies", null, commandType: CommandType.StoredProcedure);

                    ViewBag.branch = new SelectList(branch, "branch_id", "branch_name");

                    return View();
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        } 
        [Authorize]
        public ActionResult BalanceSheet()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();



                    IEnumerable<CompanyBranches> branch =
                        sqlConnection.Query<CompanyBranches>("get_all_active_companies", null, commandType: CommandType.StoredProcedure);

                    ViewBag.branch = new SelectList(branch, "branch_id", "branch_name");

                    return View();
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public JsonResult generatetrialbalance(string startDate,string endDate,string branch_id)
        {
            try
            {
                TempData["startDate"]=startDate.ToString();
                TempData["endDate"] = endDate.ToString();
                TempData["branch"] = branch_id;

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var trial_balance = sqlConnection.Query("getTrialbalance",
                       new
                       {
                           startDate=Convert.ToDateTime(startDate).ToShortDateString(),
                           endDate= Convert.ToDateTime(endDate).ToShortDateString(),
                           branch_id
                       }, commandType: CommandType.StoredProcedure);

                    return Json(trial_balance, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json("Error "+ex.Message, JsonRequestBehavior.AllowGet);

            }
        } 
        public JsonResult generateBalanceSheet(string endDate,string branch_id)
        {
            try
            {
                

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var BalanceSheet = sqlConnection.Query("generateBalanceSheet",
                       new
                       {
                           endDate= Convert.ToDateTime(endDate).ToShortDateString(),
                           branch_id
                       }, commandType: CommandType.StoredProcedure);

                    return Json(BalanceSheet, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json("Error "+ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public ActionResult IncomeStatement()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();



                    IEnumerable<CompanyBranches> branch =
                        sqlConnection.Query<CompanyBranches>("get_all_active_companies", null, commandType: CommandType.StoredProcedure);

                    ViewBag.branch = new SelectList(branch, "branch_id", "branch_name");

                    return View();
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        public JsonResult generateincomeStatement(string endDate, string branch_id)
        {
            try
            {
                

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var income_Statement = sqlConnection.Query("generateIncomeStatement",
                       new
                       {
                           endDate = Convert.ToDateTime(endDate).ToShortDateString(),
                           branch_id
                       }, commandType: CommandType.StoredProcedure);

                    return Json(income_Statement, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json("Error " + ex.Message, JsonRequestBehavior.AllowGet);

            }
        }
    }
}