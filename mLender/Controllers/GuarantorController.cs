﻿using Dapper;
using Microsoft.AspNet.Identity;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{

    public class GuarantorController : Controller
    {

        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        //private Guarantors guarantor = new Guarantors();
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();
        private LoanApplicationCombinedModel combinedModel = new LoanApplicationCombinedModel();

        private LoanApplicationRepository applicationRepository = new LoanApplicationRepository();
        oErrorLog oErrorLog = new oErrorLog();



        //
        // GET: /Guarantor/
        [Authorize]

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Guarantor/Details/5
        [Authorize]

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Guarantor/Create
        [Authorize]

        public ActionResult add()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //get drop down list for gurantors
                    IEnumerable<MembersDetails> members_list =
                        sqlConnection.Query<MembersDetails>
                        ("get_members_name_and_Number", null,
                        commandType: CommandType.StoredProcedure);
                    ViewBag.members_list =
                        new SelectList(members_list, "member_number", "fullname");

                    return View();

                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        //
        // POST: /Guarantor/Create

        [HttpPost]
        [Authorize]
        public JsonResult Add(Guarantors collection,string loan_no)
        {
            try
            {
                var results= applicationRepository.AddGuarantor(collection,loan_no);
                TempData["guarantor_success"] = "Guarantor details added successfully";
                return Json(TempData["guarantor_success"].ToString(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                TempData["guarantor_error"]= "Details not saved! " + ex.Message;
                return Json(TempData["guarantor_error"].ToString(), JsonRequestBehavior.AllowGet);

                
            }


        }

        //
        // GET: /Guarantor/Edit/5
        [Authorize]

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Guarantor/Edit/5

        [HttpPost]
        [Authorize]

        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Guarantor/Delete/5
        [Authorize]

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Guarantor/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [HttpGet]
        [Authorize]
        public JsonResult List_Guarantor(string loan_no)
        {
            try
            {
                 var list_guarantor= applicationRepository.GetLoanGuarantors(loan_no);
                return Json(list_guarantor, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        [HttpPost]
        public JsonResult Delete_Guarantor(string loan_no,string guarantor_number)
        {
            try
            {
                var delete_guarantor = applicationRepository.DeleteGuarantor(loan_no, guarantor_number);
                TempData["delete_success"] = "Guarantor details deleted successfully";
                return Json(TempData["delete_success"].ToString(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                TempData["delete_error"] = "Guarantor details not deleted,"+ex.Message;
                return Json(TempData["delete_error"].ToString(), JsonRequestBehavior.AllowGet);
                
            }

        }

    }
}
