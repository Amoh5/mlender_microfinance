﻿using Dapper;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        HomeRepository homedata = new HomeRepository();
        public ActionResult Index()
        {
            try
            {

                //if  user is an agent, then show his/her loans

                var agentIDNo = (string)Session["agentIDNo"];

                if (agentIDNo!=null)
                {
                        return RedirectToAction("index", "member");

                }
                else
                {
                                    
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var companpany_officer = sqlConnection.Query("select officer_id from officer where isactive=1").Count();
                    ViewBag.companpany_officer = companpany_officer;

                    var member_count = sqlConnection.Query("select member_number from member where is_active=1").Count();
                    ViewBag.member_count = member_count;

                    ViewBag.User_count = JiokoeDb.Users.Count();





                    return View("index");
                }
               }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        

        public ActionResult About()
        {
            try
            {
                using (sqlConnection)
                {
                    ViewBag.Message = "Your application description page.";

                    return View();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public ActionResult Contact()
        {
            try
            {
                using (sqlConnection)
                {
                    ViewBag.Message = "Your contact page.";

                    return View();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        public JsonResult getMemberAnalysis(string startDate, string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    var member_analysis= homedata.getMemberGraphDataAnalysis(startDate,toDate);
                    return Json(member_analysis, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public JsonResult getShareProjectAnalysis(string startDate, string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    var project_analysis = homedata.getShareprojectGraphDataAnalysis(startDate,toDate);
                    return Json(project_analysis, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public JsonResult getLoanIssuedAnalysis(string startDate, string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    var project_analysis = homedata.getLoanGraphDataAnalysis(startDate,toDate);
                    return Json(project_analysis, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}