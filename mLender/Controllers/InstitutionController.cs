﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using mLender.Models;
using mLender.Service;

namespace mLender.Controllers
{
    [AccessDeniedAuthorize(Roles = "Manager,Admin")]

    public class InstitutionController : Controller
    {

        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private Institution institution = new Institution();
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();

        string Filedirectory = System.Web.Hosting.HostingEnvironment.MapPath("~/ImageAndPdfFiles");


        // GET: /Institution/
        [Authorize]

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
            {
                sqlConnection.Open();

                
                    List<Institution> institutions =
                       sqlConnection.Query<Institution>("get_institution_details", commandType: CommandType.StoredProcedure).ToList();

                    return View(institutions);
                }

                
            }
            catch (Exception)
            {

                throw;
            }



        }

        //
        // GET: /Institution/Details/5
        [Authorize]

        public ActionResult Details(string id)
        {


            HttpUtility.UrlDecode(id);


            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    institution = sqlConnection.Query<Institution>("select * from institution where name='" + id + "'").FirstOrDefault();
                }
                if (institution != null)
                {
                    return View("Details", institution);
                }
                else
                {

                    return new EmptyResult();
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        //
        // GET: /Institution/Create
        [Authorize]

        public ActionResult Create()
        {

            return View();
        }

        //
        // POST: /Institution/Create

        [HttpPost]
        public JsonResult Create(Institution sacco)
        {
            try
            {

                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var institution = new Institution();
                    institution.name = sacco.name;
                    institution.address = sacco.address;
                    institution.telephone = sacco.telephone;
                    institution.email_address =sacco.email_address;
                    institution.tag_line = sacco.tag_line;
                    //institution.Logo = sacco.Logo;
                    institution.Is_Matatu_Sacco = sacco.Is_Matatu_Sacco;

                    HttpPostedFileBase postedFile = Request.Files["Logo"];

                    var photoName = Path.GetFileName(postedFile.FileName);

                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        postedFile.SaveAs(Path.Combine(Filedirectory, photoName));
                    }
                    institution.Logo = photoName;

                    var affectedRows =
                         sqlConnection.Execute("saveinstitutiondetails",
                         new { institution.name, 
                             institution.address, institution.telephone, institution.email_address,
                             institution.tag_line,institution.Logo,institution.Is_Matatu_Sacco }, commandType: CommandType.StoredProcedure);

                }
                TempData["Success"] = "Details saved succesfully";


                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);

            }
        }

        //
        // GET: /Institution/Edit/5
        [Authorize]

        public ActionResult Edit(string id)
        {
            using (sqlConnection)
            {
                sqlConnection.Open();
                ViewBag.Name = id;
                var edit_inst = sqlConnection.Query<Institution>
                    ("select * from institution where name=" + '\'' + id + '\'').FirstOrDefault();
                if (edit_inst != null)
                {
                    return View(edit_inst);

                }
                else
                {
                    //var User_message = new HttpStatusCodeResult(404, "There is no record in database to edit");
                    var User_message = new HttpNotFoundResult("There is no record in database to edit");

                    return User_message;
                }
            }

        }

        //
        // POST: /Institution/Edit/5

        [HttpPost]
        [Authorize]

        public ActionResult Edit(string id, Institution model)
        {
            try
            {
                 return Create(model);                       
                    
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Changes not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }

        }

        //
        // GET: /Institution/Delete/5
        [Authorize]

        public ActionResult Delete(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //check if the record is in db
                    institution = sqlConnection.Query<Institution>("select * from institution where name=" + '\'' + id + '\'').SingleOrDefault();
                    return View(institution);
                }
            }
            catch (Exception)
            {

                throw;
            }


        }

        //
        // POST: /Institution/Delete/5

        [HttpPost]
        [Authorize]

        public ActionResult Delete(string id, FormCollection collection)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //check if record exists
                    var institution_details = sqlConnection.Query<Institution>("select * from institution where name=" + '\'' + id + '\'');

                    if (institution_details == null)
                    {
                        var result = new HttpStatusCodeResult(999, "institution details do not exists!");
                    }
                    else
                    {
                        var delete_institution = sqlConnection.Execute("delete from institution where name=" + '\'' + id + '\'');

                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
