﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [Authorize]

    public class InsurerController : Controller
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private Insurer insurer = new Insurer();

        bool isnew = true;
        string code = "";


        //
        // GET: /Insurer/

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var list_insurer = sqlConnection.Query<Insurer>
                        ("select * from insurer where isactive=@isactive ", new { isactive = 1 }).ToList();

                    return View(list_insurer);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // GET: /Insurer/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Insurer/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Insurer/Create

        [HttpPost]
        [Authorize]

        public ActionResult Create(FormCollection collection)
        {
            try
            {
                try
                {
                    using (sqlConnection)
                    {
                        sqlConnection.Open();


                        insurer.name = collection["name"];
                        insurer.formula = collection["formula"];
                        insurer.telephone = collection["telephone"];
                        insurer.email = collection["email"];

                        var insert_insurer = sqlConnection.Execute("saveinsurerdetails",
                            new
                            {
                                insurer.name,
                                insurer.telephone,
                                insurer.email,
                                insurer.formula,
                                isnew,
                                code
                            }, commandType: CommandType.StoredProcedure);



                    }
                    TempData["Success"] = "Details saved succesfully";

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["Error"] = "Details not saved! " + ex.Message;
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Insurer/Edit/5

        public ActionResult Edit(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var edit_insurer = sqlConnection.Query<Insurer>
                        ("select * from insurer where isactive=1 and code=@id",
                        new { id }).SingleOrDefault();

                    return View(edit_insurer);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // POST: /Insurer/Edit/5

        [HttpPost]
        [Authorize]
        public ActionResult Edit(string id, FormCollection collection)
        {
            try
            {
                using (sqlConnection)
                {
                    isnew = false;
                    code = id;
                    return Create(collection);
                }
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Insurer/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Insurer/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public JsonResult BlockInsurer(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var block_insurer = sqlConnection.Execute("block_insurer",
                        new { code = id }, commandType: CommandType.StoredProcedure);

                    return Json(block_insurer, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
