﻿using Dapper;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [AccessDeniedAuthorize(Roles = "Manager,Admin")]

    public class LedgerController : Controller
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private Ledger ledger = new Ledger();
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();
        public bool IsNew = true;
        bool IsDeleted = false;
        string ledger_code = "";
        private LedgerRepository ledgerRepository = new LedgerRepository();
        oErrorLog oError = new oErrorLog();

        //
        // GET: /Ledger/
        [Authorize]

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    List<Ledger> List_ledger = new List<Ledger>();
                    List_ledger = sqlConnection.Query<Ledger>("get_ledger_Accounts",
                        null, commandType: System.Data.CommandType.StoredProcedure).ToList();
                    return View(List_ledger);
                }
            }
            catch (Exception ex)
            {

                TempData["Error"] = ex.Message;
                return RedirectToAction("Index");
            }
        }
        [HttpPut]
        [Authorize]

        public ActionResult index(string code)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var affected_row = sqlConnection.Execute
                        ("update ledger set IsDeleted=1 where code=@code",
                        new { ledger_code = code }, commandType: CommandType.Text);

                    return View("Index");

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /Ledger/Details/5
        [Authorize]

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Ledger/Create
        [Authorize]

        public ActionResult Create()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //list classes
                    IEnumerable<MainLedgerClass> main_classes = sqlConnection.Query<MainLedgerClass>
                        ("select code,class from main_ledger_classes");

                    ViewBag.main_classes = new SelectList(main_classes, "code", "class");


                    //list sub classes/groups

                    IEnumerable<Ledger_Group> ledger_group =
                        sqlConnection.Query<Ledger_Group>
                        ("select code, group_name from ledger_group order by code", null,
                        commandType: CommandType.Text).ToList();

                    ViewBag.ledger_group = new SelectList(ledger_group, "code", "group_name");

                    return View(ledger);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        //
        // POST: /Ledger/Create

        [HttpPost]
        [Authorize]

        public JsonResult Create(Ledger model)
        {
            try
            {
                var insertLedger = ledgerRepository.AddLedger(IsNew,model);
                TempData["Success"] = "Ledger details saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                oError.WriteErrorLog(ex.Message);
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Ledger/Edit/5

        public ActionResult Edit(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //list main classes
                    List<MainLedgerClass> main_classes = sqlConnection.Query<MainLedgerClass>
                       ("select code,class from main_ledger_classes").ToList();

                    ViewBag.main_classes = new SelectList(main_classes, "code", "class");

                    //list sub ledgers

                    //list sub classes/groups

                    List<Ledger_Group> ledger_group =
                        sqlConnection.Query<Ledger_Group>
                        ("select code, group_name from ledger_group", null,
                        commandType: CommandType.Text).ToList();

                    ViewBag.sub_ledger_group = new SelectList(ledger_group, "code", "group_name");

                    //return the ledger to edit
                    ledger = sqlConnection.Query<Ledger>
                       ("get_ledger_per_Id", new { @code = id }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    return View(ledger);
                }
            }
            catch (Exception ex)
            {
                oError.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);

            }
        }

        //
        // POST: /Ledger/Edit/5

        [HttpPost]
        public JsonResult Edit(string id, Ledger model)
        {
            try
            {
                ledger_code = id;
                IsNew = false;

                Create(model);
                TempData["Success"] = "Changes saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                oError.WriteErrorLog(ex.Message);
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Ledger/Delete/5

        public ActionResult Delete(string id)
        {
            return View();
        }

        //
        // POST: /Ledger/Delete/5

        [HttpPost]
        public ActionResult Delete(string id, FormCollection collection)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var affected_row = sqlConnection.Execute
                        ("update ledger set IsDeleted=1 where code=@code",
                        new { ledger_code = id }, commandType: CommandType.Text);
                    return View("Index", ledger);

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public JsonResult getSubledgers(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    IEnumerable<Ledger_Group> ledger_group =
                       sqlConnection.Query<Ledger_Group>
                       ("get_ledger_group",
                       new { @class_code = id },
                       commandType: CommandType.StoredProcedure).ToList();

                    var sub_ledgers = new SelectList(ledger_group, "code", "group_name");

                    ViewBag.Ledger_group = sub_ledgers;

                    return Json(ViewBag.Ledger_group, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public JsonResult softDelete(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var affected_row = sqlConnection.Execute
                        ("update ledger set IsDeleted=1 where code=@code",
                        new { code = id }, commandType: CommandType.Text);
                    return Json("Deleted", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

    }
}
