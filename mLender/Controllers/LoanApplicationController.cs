﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [Authorize]
    public class LoanApplicationController : Controller
    {


        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private LoanApplication loanApplication = new LoanApplication();
        private ApproveLoan approveLoan = new ApproveLoan();
        private LoanApplicationCombinedModel combinedModel = new LoanApplicationCombinedModel();
        string loan_status = "";
        string branch_name = "";
        oErrorLog oErrorLog = new oErrorLog();
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();




        // GET: /LoanApplication/
        LoanApplicationRepository loanApplicationRepository = new LoanApplicationRepository();
        [AccessDeniedAuthorize(Roles = "Admin,Manager")]
        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //populate drop down list to search member
                    IEnumerable<MembersDetails> members_list =
                       sqlConnection.Query<MembersDetails>
                       ("get_members_name_and_Number", null,
                       commandType: CommandType.StoredProcedure);
                    ViewBag.members_list =
                        new SelectList(members_list, "member_number", "fullname");

                    //find the ID of the current logged in User
                    var agentIDNo = (string)Session["agentIDNo"];

                        var loanlist = sqlConnection.Query<LoanApplication>
                       ("get_All_Applied_loans", new { agent_id=agentIDNo }, commandType: CommandType.StoredProcedure);

                        return View(loanlist);



                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }


       
        //
        // GET: /LoanApplication/Create

        [AccessDeniedAuthorize(Roles = "Agent")]

        [Authorize]
        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //populate drop down list to search member
                    IEnumerable<MembersDetails> members_list =
                       sqlConnection.Query<MembersDetails>
                       ("get_members_name_and_Number", null,
                       commandType: CommandType.StoredProcedure);
                    ViewBag.members_list =
                        new SelectList(members_list, "member_number", "fullname");

                    //get dropdown list of loan products

                    IEnumerable<LoanType> loanTypes =
                        sqlConnection.Query<LoanType>
                        ("get_loantye_name_and_code",
                        null, commandType: CommandType.StoredProcedure);
                    ViewBag.loanTypes = new SelectList(loanTypes, "code", "name");

                    //populate sropdown list for insurers

                    IEnumerable<Insurer> insurers =
                        sqlConnection.Query<Insurer>("get_insurer_name_code", null, commandType: CommandType.StoredProcedure);

                    ViewBag.insurer = new SelectList(insurers, "code", "name");

                    //populate members for guarantorship


                    var id = Session["membernumber"].ToString();

                    //get_member_sacco_profile(id);


                    return View();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /LoanApplication/Create
        [AccessDeniedAuthorize(Roles = "Agent")]
        [HttpPost]
        [Authorize]
        public JsonResult Create(LoanApplication application)
        {
            try
            {
                var insert_details = loanApplicationRepository.ApplyLoan(application);

                TempData["Success"] = "Details saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [Authorize]
        public ActionResult Details(string id)
        {
            try
            {
                sqlConnection = new SqlConnection(ConnectionString);

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var details = sqlConnection.Query<LoanApplication>("getLoanDetails",
                        new { loan_no=id }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    var member_number = details.member_number;
                    var loan_code = details.loan_code;

                    ViewBag.member = member_number;
                    ViewBag.loantype = loan_code;
                    ViewBag.loan = id;
                    return View(details);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        //
        // GET: /LoanApplication/Edit/5
        [AccessDeniedAuthorize(Roles = "Agent")]
        [Authorize]
        public ActionResult Edit(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var get_loan_to_edit = sqlConnection.Query<LoanApplication>
                        ("get_member_applied_loans_pending", new { loan_no = id },
                        commandType: CommandType.StoredProcedure);

                    return View(get_loan_to_edit);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /LoanApplication/Edit/5
        [AccessDeniedAuthorize(Roles = "Agent")]
        [HttpPost]
        public JsonResult Edit(string id, LoanApplication collection)
        {
            try
            {
               
                  var edit_loan= loanApplicationRepository.EditLoan(id, collection);
                    TempData["Success"] = "Details saved succesfully";

                    return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /LoanApplication/Delete/5
        [AccessDeniedAuthorize(Roles = "Agent")]
        [Authorize]
            [HttpPost]
        public JsonResult Delete(string loan_no)
        {
            try
            {
                var deleteLoan = loanApplicationRepository.cancelLoan(loan_no);
                TempData["Success"] = "Details saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        

        [HttpGet]
        [Authorize]
        public ActionResult Approve(string loan_no)
        {
            try
            {
                var list = loanApplicationRepository.getLoansToBeApproved(loan_no);
                return View(list);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]
        [Authorize]
        public JsonResult Approve(string loan_no, ApproveLoan collection)
        {
            try
            {
                var insert_rows = loanApplicationRepository.ApproveLoan(loan_no,collection);
                TempData["Success"] = "Details saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize]
        [HttpPost]

        public JsonResult effectLoan(string loan_no, EffectLoan model)
        {
            try
            {
                var effect_loan = loanApplicationRepository.EffectLoan(loan_no,model);
                TempData["Success"] = "Details saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }

        }



        public ActionResult get_member_sacco_profile(string member_number)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var data = sqlConnection.Query
                        ("get_member_sacco_profile",
                        new { member_number }, commandType: CommandType.StoredProcedure);

                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public JsonResult get_appliedLoans_per_Type(string loan_code)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var loanlist = sqlConnection.Query
                        ("get_All_Applied_loans_Per_LoanType", new { loan_code }, commandType: CommandType.StoredProcedure);

                    return Json(loanlist, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public JsonResult get_curent_LoanNumber(string loan_code)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var loanno = sqlConnection.Query("get_loan_number", new { loan_code },
                        commandType: CommandType.StoredProcedure).SingleOrDefault();

                    ViewBag.currentLoanNumbe = loanno.LoanNo;



                    return Json(loanno, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public JsonResult save_Payslip_Info(LoanApplication payslipInfo, string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var payslip = new LoanApplication();
                    payslip.basic_salary = payslipInfo.basic_salary;
                    payslip.house_allowance = payslipInfo.house_allowance;
                    payslip.other_allowance = payslipInfo.other_allowance;
                    payslip.other_payment = payslipInfo.other_payment;
                    payslip.total_deduction = payslipInfo.total_deduction;

                    var rows = sqlConnection.Execute("save_payslip_Info", new
                    {
                        loan_no,
                        payslip.basic_salary,
                        payslip.house_allowance,
                        payslip.other_allowance,
                        payslip.other_payment,
                        payslip.total_deduction

                    }, commandType: CommandType.StoredProcedure);
                    return Json(rows, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        [Authorize]
        public JsonResult save_guarantor(Guarantors guarantors, string loan_no)
        {
            var results = 0;
            try
            {
                
                loanApplicationRepository.AddGuarantor(guarantors, loan_no);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Details not saved! " + ex.Message;

            }

            return Json(results, JsonRequestBehavior.AllowGet);

        }
        public JsonResult get_guarantor_balance(string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var guarantor_balance = sqlConnection.Query("get_guarantor_balance", new { loan_no }, commandType: CommandType.StoredProcedure);

                    return Json(guarantor_balance, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        [Authorize]
        public JsonResult delete_guarantor(string guarantor_number)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var removed_guarantor = sqlConnection.Execute("delete_guarantor", new { guarantor_number }, commandType: CommandType.StoredProcedure);

                    return Json(removed_guarantor, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }
        [Authorize]
        public JsonResult delete_collateral(string item_code,string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();


                    var removed_guarantor = loanApplicationRepository.DeleteCollateral(item_code,loan_no);

                    return Json("Collateral deleted successfully", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }



        [Authorize]
        public JsonResult check_deposit(string member_number)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var deposit = sqlConnection.Query("get_free_deposit",
                        new { member_number }, commandType: CommandType.StoredProcedure);

                    return Json(deposit, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        [Authorize]
        public JsonResult save_collateral(LoanCollateral collection, string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var item = new LoanCollateral();
                    var systemuser = HttpContext.User.Identity.GetUserName();
                    item.code = collection.code;
                    item.Description = collection.Description;
                    item.loan_number = collection.loan_number;
                    item.Location = collection.Location;
                    item.CollateralValue = collection.CollateralValue;
                    item.CategoryCode = collection.CategoryCode;
                    item.AmountGuaranteed = collection.AmountGuaranteed;
                    item.Owner = collection.Owner;
                    item.OwnerIDNo = collection.OwnerIDNo;

                    var insertedrow = sqlConnection.Execute("save_loan_collateral", new
                    {
                        item.loan_number,
                        item.Owner,
                        item.OwnerIDNo,
                        item.code,
                        item.CategoryCode,
                        item.Description,
                        item.Location,
                        item.CollateralValue,
                        item.AmountGuaranteed,
                        audit_id = systemuser

                    }, commandType: CommandType.StoredProcedure);

                    return Json("success", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult getLoancollateral(string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var collateral = sqlConnection.Query("getLoanCollateral",new { loan_no}, commandType: CommandType.StoredProcedure);
                    
                    return Json(collateral, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public JsonResult getmember_details_approval(string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var get_loan_to_approve = sqlConnection.Query
                        ("get_member_applied_loans_pending", new { loan_no }, commandType: CommandType.StoredProcedure);


                    ViewBag.loan = get_loan_to_approve;
                    return Json(get_loan_to_approve, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpGet]
        [Authorize]
        public ActionResult effectLoan(string id, string member_number, string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    IEnumerable<EffectLoan> effectLoans = sqlConnection.Query<EffectLoan>("get_loans_to_effect",
                        new
                        {
                            loan_code = id,
                            member_number,
                            loan_no
                        }, commandType: CommandType.StoredProcedure).ToList();
                    return View(effectLoans);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public JsonResult get_member_loan_to_effect(string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var get_loan_to_effect = sqlConnection.Query
                        ("get_member_loan_to_effect", new { loan_no }, commandType: CommandType.StoredProcedure);



                    return Json(get_loan_to_effect, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public JsonResult check_loan_insurance_policy()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var get_loan_insurance_policy = sqlConnection.Query("check_insurance_policy", new { }, commandType: CommandType.StoredProcedure);
                    return Json(get_loan_insurance_policy, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public JsonResult check_loan_processing_fee_policy()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var get_processing_fee_policy = sqlConnection.Query("check_loan_processing_fee_policy", new { }, commandType: CommandType.StoredProcedure);
                    return Json(get_processing_fee_policy, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [Authorize]
        public JsonResult generate_loanschedule(decimal loanamount, decimal repayperiod,decimal interestrate, string repaymethod)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();



                    var loanschedule = sqlConnection.Query("loan_calculator", 
                        new { 
                        loanamount,
                        interestrate,
                        repayperiod,
                        repaymethod,
                        }, commandType: CommandType.StoredProcedure);
                    return Json(loanschedule, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public ActionResult loanSchedule()
        {
            try
            {
                return View();
            }
            catch (Exception ex )
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public ActionResult PendingLoans()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var list = sqlConnection.Query<LoanApplication>("getLoansPendingApproval",new{ }, commandType: CommandType.StoredProcedure).ToList();

                    return View(list);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public ActionResult disbursedLoans()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var list = sqlConnection.Query<LoanApplication>("getDisbursedLoan", new { }, commandType: CommandType.StoredProcedure).ToList();

                    return View(list);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }


        [Authorize]
        public ActionResult GetMemberLoans(string id, LoanApplication loanApplication)
        {

            Session["membernumber"] = id;


            LoanApplicationRepository loanApplicationRepository = new LoanApplicationRepository();
            string member_number = "";
            try
            {

                //get member profile


                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //get dropdown list of loan products

                    IEnumerable<Ledger> accounts =
                        sqlConnection.Query<Ledger>
                        ("get_Ledger_Banks",
                        null, commandType: CommandType.StoredProcedure);
                    ViewBag.accounts = new SelectList(accounts, "code", "name");


                    //get name of the member to display on edit title
                    var member_name = sqlConnection.Query<string>
                            ("select fullname from member where member_number=@member_number ", 
                            new { @member_number = id }).SingleOrDefault();

                    ViewBag.member = member_name;

                    Session["membername"] = member_name;

                }


                var loanList = loanApplicationRepository.GetMemberLoans(member_number = id, loanApplication);

                return View(loanList);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        [Authorize]

        public JsonResult    checkLoanStatus(string loan_no)
        {
            LoanApplicationRepository loanDetails = new LoanApplicationRepository();

            try
            {

                loanDetails.CheckLoanDetails(loan_no);

                var loanstatus = loanDetails.loan_status;
                
                return Json(loanstatus, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);  
            }
        }
        [Authorize]
        public JsonResult LoanIsSecured(string loan_no)
        {
            try
            {
                var results = loanApplicationRepository.LoanIsSecured(loan_no);

                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public ActionResult LoanAnalysis()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        public JsonResult LoanProductAnalysis(string startDate, string toDate)
        {
            HomeRepository homeRepository = new HomeRepository();
            try
            {
                var productAnalysis = homeRepository.getLoanGraphDataAnalysis(startDate,toDate);
                return Json(productAnalysis, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }
        [Authorize]
        public JsonResult LoanPerBranchAnalysis(string startDate, string toDate)
        {
            HomeRepository homeRepository = new HomeRepository();
            try
            {
                var productAnalysis = homeRepository.getLoanPerBranchGraphDataAnalysis(startDate,toDate);
                return Json(productAnalysis, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        } [Authorize]
        public JsonResult LoanInterestPerBranchAnalysis(string startDate, string toDate)
        {
            HomeRepository homeRepository = new HomeRepository();
            try
            {
                var productAnalysis = homeRepository.getLoanInterestPerBranchGraphDataAnalysis(startDate,toDate);
                return Json(productAnalysis, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }
        [Authorize]
        public JsonResult LoanIssuedvsPaidAnalysis(string startDate, string toDate)
        {
            HomeRepository homeRepository = new HomeRepository();
            try
            {
                var productAnalysis = homeRepository.getLoanIssuedVsPaidGraphDataAnalysis( startDate,  toDate);
                return Json(productAnalysis, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public JsonResult memberCountAnalysis(string startDate, string toDate)
        {
            HomeRepository homeRepository = new HomeRepository();
            try
            {
                var memberAnalysis = homeRepository.getMemberPerBranchGraphDataAnalysis(startDate, toDate);
                return Json(memberAnalysis, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        [Authorize]
        public ActionResult Collateral(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var collateral = sqlConnection.Query<LoanCollateral>("getLoanCollateral", new { loan_no=id }, commandType: CommandType.StoredProcedure);

                    ViewBag.loanno = id;
                    ViewBag.membername = Session["membername"];
                    Session["loanno"] = id;
                    ViewBag.memberNumber = Session["membernumber"];

                    return View(collateral);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
         }

        [Authorize]
        [HttpGet]
        public ActionResult AddCollateral()
        {
            try
            {
                ViewBag.loan = Session["loanno"];
                ViewBag.membername = Session["membername"];
                return View();
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public JsonResult getStatement(string loan_no,string fromDate,string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    fromDate = Convert.ToDateTime(fromDate).ToShortDateString();
                    toDate = Convert.ToDateTime(toDate).ToShortDateString();
                    var loanstatement = sqlConnection.Query("getLoanStatement", 
                        new { loan_no, fromDate, toDate }, commandType: CommandType.StoredProcedure);

                    return Json(loanstatement, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message,JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize]
        [HttpGet]
        public ActionResult LoanStatement( string id)
        {
            try
            {

                var loanStatement = new List<LoanStatement>();

                ViewBag.loanNo = id;


                return View(loanStatement);
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        public ActionResult LoanDefaulters(string branch)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    IEnumerable<CompanyBranches> branchs =
                        sqlConnection.Query<CompanyBranches>("get_all_active_companies", null, commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.branch = new SelectList(branchs, "branch_id", "branch_name");



                    if (branch==null || branch == "")
                    {
                        ViewBag.branchName = "General";
                    }
                    else
                    {
                        var company = sqlConnection.Query("get_all_active_companies", new { branch }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                        ViewBag.branchName = company.branch_name;

                    }

                    var list = sqlConnection.Query<LoanDefaulters>("getDefaulters", new { branch }, commandType: CommandType.StoredProcedure);
                    return View(list);
                }          
           
                
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public JsonResult loadAnalysis(string startDate,string toDate)
        {
            try
            {
                return memberCountAnalysis(startDate,toDate);
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message,JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize]
        public ActionResult getDueLoans(string fromDate, string toDate, string branch_id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    IEnumerable<CompanyBranches> branchs =
                       sqlConnection.Query<CompanyBranches>("get_all_active_companies", null, commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.branch = new SelectList(branchs, "branch_id", "branch_name");
                    
                    if (branch_id==""||branch_id is null)
                    {
                        ViewBag.branchName = "All Branches";
                    }
                    else
                    {
                        var company = sqlConnection.Query("get_all_active_companies", new { branch = branch_id }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                        ViewBag.branchName= company.branch_name;
                    }

                    if (fromDate is null|| fromDate=="")
                    {
                        var getList = sqlConnection.Query<DueLoans>("getDueLoans",
                       new
                       {
                           fromDate,
                           toDate,
                           branch_id

                       }, commandType: CommandType.StoredProcedure).ToList();
                        return View(getList);

                    }
                    else
                    {
                        var getList = sqlConnection.Query<DueLoans>("getDueLoans",
                        new
                        {
                            fromDate=Convert.ToDateTime(fromDate).ToShortDateString(),
                            toDate=Convert.ToDateTime(toDate).ToShortDateString(),
                            branch_id

                        }, commandType: CommandType.StoredProcedure).ToList();
                        return View(getList);

                    }



                }

            }
            catch(Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }


    }
}
