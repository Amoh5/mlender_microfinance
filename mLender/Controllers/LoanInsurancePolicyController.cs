﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    public class LoanInsurancePolicyController : Controller
    {
        private static string ConnectionString =
               ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);



        //
        // GET: /LoanInsurancePolicy/

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var list_insurancePolicy = sqlConnection.Query<LoanInsurancePolicy>("get_ListLoanInsurancePolicy", new { }, commandType: CommandType.StoredProcedure).ToList();
                    return View(list_insurancePolicy);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //
        // GET: /LoanInsurancePolicy/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /LoanInsurancePolicy/Create
        [Authorize]
        public ActionResult Create()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //populate interest ledger account..get revenue group
                    IEnumerable<Ledger> revenue_account =
                        sqlConnection.Query<Ledger>("get_ledgers", new { }, commandType: CommandType.Text);

                    ViewBag.revenue_account = new SelectList(revenue_account, "code", "name");

                    return View();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // POST: /LoanInsurancePolicy/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(LoanInsurancePolicy model)
        {

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var insurance = new LoanInsurancePolicy();

                    insurance.controll_account = model.controll_account;
                    insurance.deduct_from_loan = model.deduct_from_loan;
                    insurance.recover_from_checkoff = model.recover_from_checkoff;
                    insurance.Percentage = model.Percentage;


                    var rows_affected = sqlConnection.Execute("save_loan_insurance_policy", new
                    {
                        insurance.controll_account,
                        insurance.deduct_from_loan,
                        insurance.recover_from_checkoff,
                        insurance.Percentage

                    }, commandType: CommandType.StoredProcedure);



                    return RedirectToAction("Index");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        // GET: /LoanInsurancePolicy/Edit/5
        [Authorize]
        public ActionResult Edit()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //populate interest ledger account..get revenue group
                    IEnumerable<Ledger> revenue_account =
                        sqlConnection.Query<Ledger>("get_ledgers", new { }, commandType: CommandType.Text);

                    ViewBag.revenue_account = new SelectList(revenue_account, "code", "name");

                    var get_insurance_policy_edit = sqlConnection.Query<LoanInsurancePolicy>("get_loan_insurance_policy_edit", new { }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    return View(get_insurance_policy_edit);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // POST: /LoanInsurancePolicy/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult Edit(LoanInsurancePolicy model)
        {
            try
            {
                return Create(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        // GET: /LoanInsurancePolicy/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /LoanInsurancePolicy/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
