﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{   
    public class MemberBalancesController : Controller
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);




        //
        // GET: /MemberBalances/
        [Authorize]
        public ActionResult MemberSummaryBalances()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var getbalances = sqlConnection.Query<MemberSummaryBalance>
                        ("getMember_summary_Info", new { }, commandType: CommandType.StoredProcedure);
                    return View(getbalances);

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /MemberBalances/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }








        [HttpGet]

        [Authorize]
        public ActionResult summaryStatement()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var statement = new summaryMemberStatement();

                    //get members
                    //populate drop down list to search member
                    IEnumerable<MembersDetails> members_list =
                       sqlConnection.Query<MembersDetails>
                       ("get_members_name_and_Number", new { },
                       commandType: CommandType.StoredProcedure);
                    ViewBag.members_list =
                        new SelectList(members_list, "member_number", "fullname");


                    return View();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public JsonResult getSummary_statement(string member_number, string firstDate, string secondDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    DateTime startDate = Convert.ToDateTime(firstDate);

                    DateTime endDate = Convert.ToDateTime(secondDate);
                    var summary_statement = sqlConnection.Query
                        ("get_summary_memberStatement", new
                        {
                            member_number,
                            startDate,
                            endDate
                        }, commandType: CommandType.StoredProcedure);
                    return Json(summary_statement, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public JsonResult getDeposit_Statement(string member_number, string firstDate, string secondDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    DateTime startDate = Convert.ToDateTime(firstDate);

                    DateTime endDate = Convert.ToDateTime(secondDate);
                    var deposit_statement = sqlConnection.Query
                        ("getMemberdeposit_statement", new
                        {
                            member_number,
                            startDate,
                            endDate
                        }, commandType: CommandType.StoredProcedure);
                    return Json(deposit_statement, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        public ActionResult LoanStatement(string loan_no)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var loanStatements= sqlConnection.Query<LoanStatement>
                        ("getMember_summary_Info", new { }, commandType: CommandType.StoredProcedure);
                    return View(loanStatements);

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

    }
}
