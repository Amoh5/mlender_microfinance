﻿using Dapper;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    public class MemberController : Controller
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private MembersDetails membersDetails = new MembersDetails();

        MemberDataRepository memberdatarepository = new MemberDataRepository();

        oErrorLog oErrorLog = new oErrorLog();

        //
        // GET: /Member/

        [Authorize]

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //find the ID of the current logged in User
                    var agentIDNo = (string)Session["agentIDNo"];

                    ViewBag.agentID = agentIDNo;

                    List<MembersDetails> membersDetails =
                         sqlConnection.Query<MembersDetails>("Get_Member_Summary_details", new{agentIDNo }, commandType: CommandType.StoredProcedure).ToList();
                    return View(membersDetails);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /Member/Details/5
        [Authorize]

        public ActionResult Details(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //check if id exists
                    var mem_no = sqlConnection.Query
                        ("select member_number from member where member_number=@id", new { id }).SingleOrDefault();
                    if (mem_no != null)
                    {
                        var member_Details = sqlConnection.Query<MembersDetails>
                                                ("Get_Member_details_per_Member", new { @member_number = id }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                        var member_name = sqlConnection.Query<string>
                            ("get_Member_FullName", new { @member_number = id },commandType:CommandType.StoredProcedure).SingleOrDefault();

                        ViewBag.member = member_name;
                        return View(member_Details);
                    }
                    else
                    {
                        return HttpNotFound("Member No.: " + id + " cannot be found in the database!");
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        //
        // GET: /Member/Create
        [Authorize]
        [AccessDeniedAuthorize(Roles="Agent")]
        public ActionResult Add()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //populate companies/branches
                    IEnumerable<CompanyBranches> branch_id =
                        sqlConnection.Query<CompanyBranches>("select branch_id, branch_name from branch where is_active = @Is_Active", new { is_Active = 1 });
                    ViewBag.branch_id = new SelectList(branch_id, "branch_id", "branch_name");


                    //populate company Officer
                    IEnumerable<CompanyOfficer> officer_id =
                        sqlConnection.Query<CompanyOfficer>
                        ("get_company_officer", new { },commandType:CommandType.StoredProcedure);
                    ViewBag.officer_id = new SelectList(officer_id, "officer_id", "fullname");

                    return View("add");
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /Member/Create

        [HttpPost]
        [Authorize]

        public JsonResult Add(string id,MembersDetails model)
        {
            
                try
                {

                     memberdatarepository.AddMember(id,model);

                    TempData["Success"] = "Details saved succesfully";

                    return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {

                    oErrorLog.WriteErrorLog(ex.Message);
                    TempData["Error"] = "Details not saved! " + ex.Message;
                    return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);

                }
           
        }

        //
        // GET: /Member/Edit/5
        [Authorize]

        public ActionResult Edit(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    
                    //populate branches/companies in dropdownlist
                    List<CompanyBranches> companies =
                        sqlConnection.Query<CompanyBranches>
                        ("get_branch_id_and_name",new { },commandType:CommandType.StoredProcedure).ToList();


                    ViewBag.companies = new SelectList(companies, "branch_id", "branch_name");

                    //populate company Officer
                    List<CompanyOfficer> officer =
                        sqlConnection.Query<CompanyOfficer>
                        ("get_company_officer", new { }, commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.officer= new SelectList(officer, "officer_id", "fullname");

                    //get name of the member to display on edit title
                    var member_name = sqlConnection.Query<string>
                            ("select first_name+' '+other_name from member where member_number=@member_number ", new { @member_number = id }).SingleOrDefault();
                    ViewBag.member = member_name;




                    var get_member_edit = sqlConnection.Query<MembersDetails>
                        ("Get_Member_details_per_Member", new { @member_number = id },
                        commandType: CommandType.StoredProcedure).SingleOrDefault();
                    return View(get_member_edit);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /Member/Edit/5

        [HttpPost]
        [Authorize]

        public JsonResult Edit(string memberno, MembersDetails model)
        {
            try
            {
                

                     memberdatarepository.EditMember(memberno,model);

                    TempData["Success"] = "Changes saved succesfully";

                    return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

                
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /Member/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Member/Delete/5

        [HttpPost]
        [Authorize]

        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [Authorize]

        public ActionResult UploadFiles(HttpPostedFileBase file)
        {
            return View();
        }

        [Authorize]
        public ActionResult GetMemberLoans(string id, LoanApplication loanApplication)
        {
            LoanApplicationRepository loanApplicationRepository = new LoanApplicationRepository();
            string member_number = "";
            try
            {
                var loanList = loanApplicationRepository.GetMemberLoans(member_number=id, loanApplication);

                return View(loanList);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
