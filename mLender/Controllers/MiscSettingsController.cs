﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    public class MiscSettingsController : Controller
    {
        private static string ConnectionString =
               ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        private MiscellaneousSettings miscellaneous = new MiscellaneousSettings();

        private bool is_new = true;

        //
        // GET: /MiscSettings/
        [Authorize]
        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();


                    var list_charge = sqlConnection.Query<MiscellaneousSettings>("get_miscellaneous_charges",
                        new { }, commandType: CommandType.StoredProcedure).ToList();

                    return View(list_charge);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // GET: /MiscSettings/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /MiscSettings/Create
        [HttpGet]
        [Authorize]
        public ActionResult Create()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //populate interest ledger account..get revenue group
                    IEnumerable<Ledger> revenue_account =
                        sqlConnection.Query<Ledger>("get_ledgers", new { }, commandType: CommandType.Text);

                    ViewBag.revenue_account = new SelectList(revenue_account, "code", "name");

                    //get charges type
                    IEnumerable<ChargeTypes> charges = sqlConnection.Query<ChargeTypes>("get_charges", new { },
                        commandType: CommandType.StoredProcedure);

                    ViewBag.charges = new SelectList(charges, "code", "type");





                    return View();

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        //
        // POST: /MiscSettings/Create

        [HttpPost]
        [Authorize]
        public ActionResult Create(MiscellaneousSettings miscellaneous)
        {
            var settings = new MiscellaneousSettings();
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    settings.Name = miscellaneous.Name;
                    settings.Account_No = miscellaneous.Account_No;
                    settings.default_amount = miscellaneous.default_amount;
                    settings.Percentage = miscellaneous.Percentage;
                    settings.type = miscellaneous.type;

                    var rows_affected = sqlConnection.Execute("save_miscellaneous_Charges", new
                    {
                        settings.Name,
                        settings.Account_No,
                        settings.default_amount,
                        settings.Percentage,
                        settings.type,
                        is_new
                    }, commandType: CommandType.StoredProcedure);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        // GET: /MiscSettings/Edit/5
        [Authorize]
        public ActionResult Edit(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //populate interest ledger account..get revenue group
                    IEnumerable<Ledger> revenue_account =
                        sqlConnection.Query<Ledger>("select l.code code, l.name  from ledger l join " +
                        "ledger_group lg on lg.code = l.ledger_group and lg.class_code =@code ", new { code = 4000 }, commandType: CommandType.Text);

                    ViewBag.revenue_account = new SelectList(revenue_account, "code", "name");
                    //get charges type
                    IEnumerable<ChargeTypes> charges = sqlConnection.Query<ChargeTypes>("get_charges", new { },
                        commandType: CommandType.StoredProcedure);

                    ViewBag.charges = new SelectList(charges, "code", "type");

                    var charge = sqlConnection.Query<MiscellaneousSettings>
                        ("get_miscellaneous_charge", new { type = id }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    return View(charge);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // POST: /MiscSettings/Edit/5

        [HttpPost]
        [Authorize]
        public ActionResult Edit(string id, MiscellaneousSettings miscellaneous)
        {
            try
            {
                sqlConnection.Open();

                is_new = false;
                miscellaneous.type = id;
                return Create(miscellaneous);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        // GET: /MiscSettings/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /MiscSettings/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
