﻿using Dapper;
using Microsoft.AspNet.Identity;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [Authorize]
    [AccessDeniedAuthorize(Roles = "Manager,Admin")]

    public class PaymentController : Controller
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);


        oErrorLog oErrorLog = new oErrorLog();

        MemberDataRepository  memberDataRepository=new MemberDataRepository();
        string AuditID = GetCurrentUserService.CurrentUser();


        // GET: /Payment/


        [Authorize]
        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var list_receipts = sqlConnection.Query<otherPayments>("getPaymnets", null).ToList();
                    return View(list_receipts);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /Payment/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Payment/Create
        [Authorize]

        public ActionResult Member()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //get members in the voucher table
                    IEnumerable<MembersDetails> members =
                        sqlConnection.Query<MembersDetails>("get_members_In_Voucher", null, commandType: CommandType.StoredProcedure);

                    ViewBag.members = new SelectList(members, "member_number", "fullname");

                    //get ledger bank to credit

                    IEnumerable<Ledger> ledgers =
                        sqlConnection.Query<Ledger>("get_Ledger_Banks", null, commandType: CommandType.StoredProcedure);

                    ViewBag.ledgers = new SelectList(ledgers, "code", "name");

                    IEnumerable<PaymentMode> payment_mode = sqlConnection.Query<PaymentMode>("getPayment_mode", null, commandType: CommandType.StoredProcedure);

                    ViewBag.payment_mode = new SelectList(payment_mode, "code", "name");

                    return View();
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }

        }

        //
        // POST: /Payment/Create
        [Authorize]
        [HttpPost]
        public JsonResult Member(MemberPayment payment)
        {
            try
            {

               var insert_payment= memberDataRepository.LoanPayment(payment);                   
               return Json(insert_payment, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /Payment/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Payment/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Payment/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Payment/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult getVouchers(string member_number)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var vouchers = sqlConnection.Query("getVouchers_per_member", new { member_number }, commandType: CommandType.StoredProcedure);
                    return Json(vouchers, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        //handles refunds of the memeber
        [Authorize]
        [HttpGet]
        public ActionResult Refunds()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //GET ALL active members
                    IEnumerable<MembersDetails> members = sqlConnection.Query<MembersDetails>
                    ("get_members_name_and_Number", null, commandType: CommandType.StoredProcedure);

                    ViewBag.members = new SelectList(members, "member_number", "fullname");


                    //get ledger bank to credit
                    IEnumerable<Ledger> ledgers =
                        sqlConnection.Query<Ledger>("get_Ledger_Banks", null, commandType: CommandType.StoredProcedure);

                    ViewBag.ledgers = new SelectList(ledgers, "code", "name");

                    //GET REFUND TYPES
                    IEnumerable<RefundTypes> refundTypes =
                        sqlConnection.Query<RefundTypes>("getRefundType", null, commandType: CommandType.StoredProcedure);
                    ViewBag.refundTypes = new SelectList(refundTypes, "code", "RefundType");

                    //get payment modes
                    IEnumerable<PaymentMode> paymentModes =
                            sqlConnection.Query<PaymentMode>("getPayment_mode", null, commandType: CommandType.StoredProcedure);

                    ViewBag.paymentModes = new SelectList(paymentModes, "code", "Name");

                }
                return View();
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult saveRefund(MemberRefund model)
        {
            try
            {           
                    
                     
                    var insertRefund =memberDataRepository.RefundMember(model);
                        
                    return Json(insertRefund, JsonRequestBehavior.AllowGet);

                
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetPaymentsThroughMpesa()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                IEnumerable<CompanyBranches> branch =
                        sqlConnection.Query<CompanyBranches>("get_all_active_companies", null, commandType: CommandType.StoredProcedure);

                ViewBag.branch = new SelectList(branch, "branch_id", "branch_name");
                return View("GetPaymentsThroughMpesa");
                }

            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public JsonResult GetMpesaVouchers(string startDate,string endDate,string branch_id)
        {
            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();
                    var list = sqlConnection.Query("getMpesaVouchers", new {
                        startDate=Convert.ToDateTime(startDate).ToShortDateString()
                        ,endDate=Convert.ToDateTime(endDate).ToShortDateString()
                        ,branch_id }, commandType: CommandType.StoredProcedure).ToList();
                    return Json(list,JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        //INSERT TO TEMPMPESAVOUCHER
        [Authorize]
        public JsonResult saveMpesaTempVoucher(DateTime voucher_date, string voucher_no, decimal amount, string payee,
            string loan_no, string description) 
        {

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                     var insertTemp = sqlConnection.Execute("insertTempMpesaVouchers",
                        new {
                            voucher_date,
                            voucher_no,
                            amount,
                            payee,
                            loan_no, 
                            description
                        }, commandType: CommandType.StoredProcedure);

                    return Json("Selected payment(s) saved successfully", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                return Json("Selected payment(s) not saved "+ex.Message, JsonRequestBehavior.AllowGet);

            }

        }

        [Authorize]
        public JsonResult getSelectedVouchers()
        {
           
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var list = sqlConnection.Query("getConfirmedMpesaVouchers", new { }, commandType: CommandType.StoredProcedure).ToList();
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        public JsonResult BatchPayment(MemberPayment payment)
        {
            try
            {
                var insert_payment = memberDataRepository.LoanPayment(payment);
                return Json("Selected payment(s) processed successfully", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }
        [Authorize]
        [HttpGet]
        public ActionResult otherPayments()
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //get ledger bank to credit

                    IEnumerable<Ledger> ledgers =
                        sqlConnection.Query<Ledger>("get_Ledger_Banks", null, commandType: CommandType.StoredProcedure);
                    ViewBag.ledgers = new SelectList(ledgers, "code", "name");
                }

                return View();
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public JsonResult otherPayments(otherPayments payment)
        {
            try
            {
                var p = new otherPayments();
                p.member_number = payment.member_number;
                p.payment_date = payment.payment_date;
                p.voucher_no = payment.voucher_no;
                p.reference_no = payment.reference_no;
                p.payment_mode = "";
                p.bank_account = payment.bank_account;
                p.amount = payment.amount;
                var audit_id = AuditID;
                p.debit_account = payment.debit_account;
                p.narration = payment.narration;

                using (sqlConnection)
                {
                    sqlConnection.Open();


                    var insert_payment =
                        sqlConnection.Execute("saveOtherPayments",
                        new
                        {
                            p.member_number,
                            p.bank_account,
                            p.payment_date,
                            p.amount,
                            p.voucher_no,
                            p.reference_no,
                            p.payment_mode,
                            audit_id,
                            p.debit_account,
                            p.narration



                        }, commandType: CommandType.StoredProcedure);
                }

                return Json("Payment processed successfully", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

    }
}
