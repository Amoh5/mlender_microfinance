﻿using Dapper;
using Microsoft.AspNet.Identity;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [AccessDeniedAuthorize(Roles = "Manager,Admin,Agent")]


    public class ReceiptController : Controller
    {

        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();

        oErrorLog oErrorLog = new oErrorLog();

        DateTime audit_time = DateTime.Now;


        [Authorize]
        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var list_receipts = sqlConnection.Query<Receipt>("getReceipts", null).ToList();
                    return View(list_receipts);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult CombinedReceipt()
        {
            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();

                    //get ledger bank to debit

                    IEnumerable<Ledger> ledgers =
                        sqlConnection.Query<Ledger>("get_Ledger_Banks", null, commandType: CommandType.StoredProcedure);

                    ViewBag.ledgers = new SelectList(ledgers, "code", "name");


                    //GET ALL active members

                    IEnumerable<MembersDetails> members = sqlConnection.Query<MembersDetails>
                    ("get_members_name_and_Number", null, commandType: CommandType.StoredProcedure);

                    ViewBag.members = new SelectList(members, "member_number", "fullname");


                    return View();
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult CombinedReceipt(Receipt model)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var receipt = new Receipt();
                    var systemuser = HttpContext.User.Identity.GetUserName();
                    receipt.amount = model.amount;
                    receipt.debit_account = model.debit_account;
                    receipt.loan_no = model.loan_no;
                    receipt.member_number = model.member_number;
                    receipt.receipt_date = model.receipt_date;
                    receipt.reference_number = model.reference_number;
                    receipt.type = model.type;
                    var audit_id = systemuser;

                    var save_receipt = sqlConnection.Execute("[saveCombinedReceipt]", new
                    {
                        receipt.member_number,
                        receipt.loan_no,
                        receipt.type,
                        receipt.amount,
                        receipt.receipt_date,
                        receipt.debit_account,
                        receipt.reference_number,
                        audit_id,
                        audit_time
                    }, commandType: CommandType.StoredProcedure);


                    return Json("Receipt made successfully", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /ReceiptContribution/Create

        [Authorize]

        public ActionResult Create()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //get member to recipt
                    IEnumerable<MembersDetails> members_list = sqlConnection.Query
                        <MembersDetails>("get_members_name_and_Number", null,
                       commandType: CommandType.StoredProcedure);
                    ViewBag.members_list =
                        new SelectList(members_list, "member_number", "fullname");

                    //get ledget to debit

                    IEnumerable<Ledger> ledgers = sqlConnection.Query<Ledger>
                            ("get_ledger_to_Debit", null, commandType: CommandType.StoredProcedure);
                    ViewBag.debit_ledgers = new SelectList(ledgers, "code", "name");

                    return View();
                }

            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        

        //
        // GET: /ReceiptContribution/Edit/5
        [Authorize]

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /ReceiptContribution/Edit/5

        [HttpPost]
        [Authorize]

        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ReceiptContribution/Delete/5
        [Authorize]

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /ReceiptContribution/Delete/5

        [HttpPost]
        [Authorize]

        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public JsonResult get_Member_expectations(string member_number,string from_date)
        {
            try
            {
                DateTime date = Convert.ToDateTime(from_date);
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    
                    var expectations = sqlConnection.Query("get_Member_Expectation",
                        new { 
                            member_number,
                            date 
                        }, commandType: CommandType.StoredProcedure);
                    return Json(expectations, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        //[Authorize]
        [HttpGet]
        public ActionResult NonMemberReceipt()
        {
           SqlConnection sqlConnection = new SqlConnection(ConnectionString);

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //get ledger bank to credit

                    IEnumerable<Ledger> ledgers =
                        sqlConnection.Query<Ledger>("get_Ledger_Banks", null, commandType: CommandType.StoredProcedure);
                    ViewBag.ledgers = new SelectList(ledgers, "code", "name");
                }

                return View();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        } 
        [HttpGet]
        public ActionResult OtherReceipts()
        {
           SqlConnection sqlConnection = new SqlConnection(ConnectionString);

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //get ledger bank to credit

                    IEnumerable<Ledger> ledgers =
                        sqlConnection.Query<Ledger>("get_Ledger_Banks", null, commandType: CommandType.StoredProcedure);
                    ViewBag.ledgers = new SelectList(ledgers, "code", "name");
                }

                return View();
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        [HttpPost]
        public ActionResult NonMemberReceipt(NonMemberWalletReceipt model)
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);
            try
                {
                    using (sqlConnection)
                    {
                        sqlConnection.Open();

                        var receipt = new NonMemberWalletReceipt();
                        var systemuser = HttpContext.User.Identity.GetUserName();
                        receipt.Credit = model.Credit;
                        receipt.debit_account = model.debit_account;
                        receipt.IDNo = model.IDNo;
                        receipt.MobileNo = model.MobileNo;
                        receipt.receipt_date = model.receipt_date;
                        receipt.reference_number = model.reference_number;
                        receipt.type = "Wallet";
                        var audit_id = systemuser;

                        var save_receipt = sqlConnection.Execute("[saveCombinedReceipt]", new
                        {
                            member_number = receipt.IDNo,
                            loan_no = receipt.MobileNo,
                            receipt.type,
                            amount = receipt.Credit,
                            receipt.receipt_date,
                            receipt.debit_account,
                            receipt.reference_number,
                            audit_id,
                            audit_time
                        }, commandType: CommandType.StoredProcedure);

                        IEnumerable<Ledger> ledgers =
                            sqlConnection.Query<Ledger>("get_Ledger_Banks", null, commandType: CommandType.StoredProcedure);
                        ViewBag.ledgers = new SelectList(ledgers, "code", "name");

                        //return View(model);
                        return RedirectToAction("index");
                    }

                }
                catch (Exception ex)
                {
                    oErrorLog.WriteErrorLog(ex.Message);
                    throw new Exception(ex.Message);
                }
            } 
        [HttpPost]
        public JsonResult OtherReceipts(OtherReceipts model)
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);
            try
                {
                    using (sqlConnection)
                    {
                        sqlConnection.Open();

                        var receipt = new OtherReceipts();
                        var systemuser = HttpContext.User.Identity.GetUserName();
                        receipt.Amount = model.Amount;
                        receipt.description = model.description;
                        receipt.debit_account = model.debit_account;
                        receipt.receipt_date = model.receipt_date;
                        receipt.reference_number = model.reference_number;
                        receipt.type = "Other Receipts";
                        var audit_id = systemuser;
                        receipt.credit_account = model.credit_account;
                        receipt.Name = model.Name;
                        var save_receipt = sqlConnection.Execute("[saveCombinedReceipt]", new
                        {
                            member_number = receipt.Name,
                            loan_no ="",
                            receipt.type,
                            amount = receipt.Amount,
                            receipt.receipt_date,
                            receipt.debit_account,
                            receipt.reference_number,
                            audit_id,
                            audit_time,
                            receipt.description,
                            receipt.credit_account

                        }, commandType: CommandType.StoredProcedure);

                        

                    return Json("Receipt posted successfully", JsonRequestBehavior.AllowGet);

                }

            }
                catch (Exception ex)
                {
                    oErrorLog.WriteErrorLog(ex.Message);
                    throw new Exception(ex.Message);
                }
            }

    }
}
