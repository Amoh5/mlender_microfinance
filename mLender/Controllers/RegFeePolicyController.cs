﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    public class RegFeePolicyController : Controller
    {
        private static string ConnectionString =
               ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        private RegFeePolicy RegFee = new RegFeePolicy();

        //private bool is_new = true;
        //
        // GET: /RegFeePolicy/

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var regpolicy = sqlConnection.Query<RegFeePolicy>("get_ListRegFeePolicy", new { }, commandType: CommandType.StoredProcedure).ToList();

                    return View(regpolicy);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // GET: /RegFeePolicy/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /RegFeePolicy/Create
        [Authorize]
        public ActionResult Create()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    //populate interest ledger account..get revenue group
                    IEnumerable<Ledger> revenue_account =
                        sqlConnection.Query<Ledger>("get_ledgers", new { }, commandType: CommandType.Text);

                    ViewBag.revenue_account = new SelectList(revenue_account, "code", "name");

                    return View();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // POST: /RegFeePolicy/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(RegFeePolicy model)
        {
            var reg = new RegFeePolicy();
            if (ModelState.IsValid)
            {
                try
                {
                    using (sqlConnection)
                    {

                        reg.credit_account = model.credit_account;
                        reg.deduct_from_loan = model.deduct_from_loan;
                        reg.mandatory_on_registration = model.mandatory_on_registration;
                        reg.recover_from_checkoff = model.recover_from_checkoff;
                        reg.recover_from_deposit = model.recover_from_deposit;

                        var row = sqlConnection.Execute("save_RegFeePolicy",

                            new
                            {
                                reg.credit_account,
                                reg.mandatory_on_registration,
                                reg.recover_from_deposit,
                                reg.deduct_from_loan,
                                reg.recover_from_checkoff,
                            },
                            commandType: CommandType.StoredProcedure);



                    }


                }

                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return RedirectToAction("Index");

        }

        //
        // GET: /RegFeePolicy/Edit/5

        public ActionResult Edit()
        {
            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();
                    //populate interest ledger account..get revenue group
                    IEnumerable<Ledger> revenue_account =
                        sqlConnection.Query<Ledger>("get_ledgers", new { }, commandType: CommandType.Text);

                    ViewBag.revenue_account = new SelectList(revenue_account, "code", "name");

                    var edit_regFee = sqlConnection.Query<RegFeePolicy>("getRegFeePolicy", new { }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return View(edit_regFee);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //
        // POST: /RegFeePolicy/Edit/5

        [HttpPost]
        public ActionResult Edit(RegFeePolicy model)
        {
            try
            {
                return Create(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        // GET: /RegFeePolicy/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /RegFeePolicy/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
