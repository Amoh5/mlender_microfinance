﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Dapper;
using Microsoft.Reporting.WebForms;
using mLender.Models;
using mLender.PDF;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace mLender.Controllers
{
    [Authorize]
    [AccessDeniedAuthorize(Roles = "Manager,Admin")]

    public class ReportController : Controller
    {

        private static string ConnectionString =
                ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        EmailStatementsMonthly statementsMonthly = new EmailStatementsMonthly();

        oErrorLog oErrorLog = new oErrorLog();
        //
        // GET: /Report/

        public ActionResult membersDetails()
        {
            try
            {
                sqlConnection.Open();
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.SizeToReportContent = true;


                //get header deatils

                List<Institution> institution_details = sqlConnection.Query<Institution>("get_institution_details").ToList();


                List<MembersDetails> membersdetails = sqlConnection.Query<MembersDetails>
                    ("get_MemberListReport", commandType: CommandType.StoredProcedure).ToList();





                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("MemberDetails", membersdetails));

                reportViewer.LocalReport.DataSources.Add(new ReportDataSource("Institution", institution_details));






                reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\Member_details.rdlc";



                ViewBag.ReportViewer = reportViewer;

                return View("membersDetails");
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());

                throw new Exception(ex.Message);
            }
        }


        //
        // GET: /Report/Details/5

        public ActionResult summarybalancesReport()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    ReportViewer reportViewer = new ReportViewer();
                    reportViewer.ProcessingMode = ProcessingMode.Local;
                    reportViewer.SizeToReportContent = true;


                    List<MemberSummaryBalance> summarybalances =
                        sqlConnection.Query<MemberSummaryBalance>("getMember_summary_Info", null, commandType: CommandType.StoredProcedure).ToList();

                    //get header deatils

                    List<Institution> institution_details = sqlConnection.Query<Institution>("get_institution_details").ToList();

                    //get data source to report viewer
                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("member_summary_balance", summarybalances));
                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("institution", institution_details));




                    //map report to the folder location
                    reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\member_summary_balance.rdlc";

                    ViewBag.ReportViewer = reportViewer;

                    return View("summarybalancesReport");

                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());

                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /Report/Create

        public ActionResult summary_statement()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var model = new MemberStatementReport();
                    ReportViewer reportViewer = new ReportViewer();
                    reportViewer.ProcessingMode = ProcessingMode.Local;
                    reportViewer.SizeToReportContent = true;


                    //get drop down list for gurantors
                    IEnumerable<MembersDetails> member_statement =
                        sqlConnection.Query<MembersDetails>
                        ("get_members_name_and_Number", null,
                        commandType: CommandType.StoredProcedure);

                    ViewBag.member_statement =
                        new SelectList(member_statement, "member_number", "fullname");

                    //reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\";

                    ViewBag.ReportViewer = reportViewer;


                    return View(model);

                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /Report/Create
        [Authorize]
        [HttpPost]
        public ActionResult summary_statement(string member_number, DateTime startDate, DateTime endDate, MemberStatementReport memberStatement)
        {
            try
            {


                using (sqlConnection)
                {
                    sqlConnection.Open();
                    ReportViewer reportViewer = new ReportViewer();
                    reportViewer.ProcessingMode = ProcessingMode.Local;
                    reportViewer.SizeToReportContent = true;

                    //string member_number = "j00001";
                    //DateTime startDate = DateTime.Now.AddMonths(-4);
                    //DateTime endDate = DateTime.Now;


                    List<summaryMemberStatement> summarystatement =
                        sqlConnection.Query<summaryMemberStatement>("get_summary_memberStatement",
                        new { member_number, startDate, endDate }, commandType: CommandType.StoredProcedure).ToList();

                    //get header deatils

                    List<Institution> institution_details = sqlConnection.Query<Institution>("get_institution_details").ToList();

                    //get member details to embedd in the report

                    List<MembersDetails> member = sqlConnection.Query<MembersDetails>
                        ("Get_Member_details_per_Member", new { member_number }, commandType: CommandType.StoredProcedure).ToList();



                    //add datasource to the report
                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("member_summary_statement", summarystatement));
                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("institution", institution_details));
                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("member_profile", member));






                    //map report to the folder location
                    reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\member_summary_statement.rdlc";


                    reportViewer.LocalReport.ReportPath = reportViewer.LocalReport.ReportPath.Replace("\\", @"\");

                    ViewBag.ReportViewer = reportViewer;

                    //reload drop down list for gurantors
                    IEnumerable<MembersDetails> member_statement =
                        sqlConnection.Query<MembersDetails>
                        ("get_members_name_and_Number", null,
                        commandType: CommandType.StoredProcedure);

                    ViewBag.member_statement =
                        new SelectList(member_statement, "member_number", "fullname");
                    return View(memberStatement);
                }

            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /Report/Edit/5

        public ActionResult cashbook1()
        {
            try
            {
                var ledger = TempData["ledger"].ToString();
                var startDate = TempData["startDate"].ToString();
                var endDate = TempData["endDate"].ToString();

                using (sqlConnection)
                {

                    sqlConnection.Open();
                    ReportViewer reportViewer = new ReportViewer();
                    reportViewer.ProcessingMode = ProcessingMode.Local;
                    reportViewer.SizeToReportContent = true;

                    List<Cashbook> cashbook =
                        sqlConnection.Query<Cashbook>("generateCashbookTransactions",
                        new
                        {
                            ledger,
                            startDate = Convert.ToDateTime(startDate).ToShortDateString(),
                            endDate = Convert.ToDateTime(endDate).ToShortDateString()
                        }, commandType: CommandType.StoredProcedure).ToList();

                    // get header deatils

                    List<Institution> institution_details = sqlConnection.Query<Institution>("get_institution_details").ToList();


                    //add datasource to the report
                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("institution", institution_details));

                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("cashbook", cashbook));

                    //map report to the folder location
                    reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\cashbook.rdlc";


                    reportViewer.LocalReport.ReportPath = reportViewer.LocalReport.ReportPath.Replace("\\", @"\");

                    ViewBag.ReportViewer = reportViewer;

                    return View(cashbook);

                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /Report/Edit/5

        [Authorize]
        public ActionResult Trialbalance1()
        {
            try
            {
                var startDate = TempData["startDate"].ToString();
                var endDate = TempData["endDate"].ToString();
                using (sqlConnection)
                {

                    sqlConnection.Open();
                    ReportViewer reportViewer = new ReportViewer();
                    reportViewer.ProcessingMode = ProcessingMode.Local;
                    reportViewer.SizeToReportContent = true;


                    List<trialBalance> trialbalance =
                        sqlConnection.Query<trialBalance>("getTrialbalance",
                        new
                        {
                            startDate=Convert.ToDateTime(startDate).ToShortDateString(),
                            endDate= Convert.ToDateTime(endDate).ToShortDateString()
                        }, commandType: CommandType.StoredProcedure).ToList();

                    // get header deatils

                    List<Institution> institution_details = sqlConnection.Query<Institution>("get_institution_details").ToList();


                    //add datasource to the report
                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("institution", institution_details));

                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource("trialbalance", trialbalance));

                    //map report to the folder location
                    reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\trialbalance.rdlc";


                    reportViewer.LocalReport.ReportPath = reportViewer.LocalReport.ReportPath.Replace("\\", @"\");

                    ViewBag.ReportViewer = reportViewer;

                    return View(trialbalance);


                }

            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /Report/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Report/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        [Authorize]

        public ActionResult LoanStatementPerLoan(string loan_no, string fromDate, string toDate)
        {
            try
            {
                                    

                fromDate = Convert.ToDateTime(fromDate).ToShortDateString();
                toDate = Convert.ToDateTime(toDate).ToShortDateString();

                ReportDocument rd = new ReportDocument();
                rd.Load(Request.MapPath(Request.ApplicationPath) + @"Reports\LoanStatementPerLoan.rpt");

                

                rd.SetParameterValue("@loan_no", loan_no);
                rd.SetParameterValue("@fromDate", fromDate);
                rd.SetParameterValue("@toDate", toDate);

                ConnectionInfo info = new ConnectionInfo();
                info.DatabaseName = "JIOKOE";
                info.UserID = "sa";
                info.Password = "k@r1bu";

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();


                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                
                rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, 
                System.Web.HttpContext.Current.Response, false, "LoanStatementPerLoan");

                string LoanstatementPdfPaths= Request.MapPath(Request.ApplicationPath) + @"LoanStatements\";
                rd.ExportToDisk(ExportFormatType.PortableDocFormat, LoanstatementPdfPaths + loan_no + ".pdf");//export to disk then send email


                return Json(new { result = 0, err = "" }, JsonRequestBehavior.AllowGet);


            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public ActionResult cashbook(string ledger,string startDate,string endDate,string branch_id )
        {
            try
            {


                startDate = Convert.ToDateTime(startDate).ToShortDateString();
                endDate = Convert.ToDateTime(endDate).ToShortDateString();

                ReportDocument rd = new ReportDocument();
                rd.Load(Request.MapPath(Request.ApplicationPath) + @"Reports\cashbook.rpt");



                rd.SetParameterValue("@ledger", ledger);
                rd.SetParameterValue("@startDate", startDate);
                rd.SetParameterValue("@endDate", endDate);
                rd.SetParameterValue("@branch_id", branch_id);

                ConnectionInfo info = new ConnectionInfo();
                info.DatabaseName = "JIOKOE";
                info.UserID = "sa";
                info.Password = "k@r1bu";

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();


                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "Cashbook");
                return Json(new { result = 0, err = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public ActionResult TrialBalance(string startDate, string endDate, string branch_id)
        {
            try
            {


                startDate = Convert.ToDateTime(startDate).ToShortDateString();
                endDate = Convert.ToDateTime(endDate).ToShortDateString();

                ReportDocument rd = new ReportDocument();
                rd.Load(Request.MapPath(Request.ApplicationPath) + @"Reports\TrialBalance.rpt");



                rd.SetParameterValue("@startDate", startDate);
                rd.SetParameterValue("@endDate", endDate);
                rd.SetParameterValue("@branch_id", branch_id);

                ConnectionInfo info = new ConnectionInfo();
                info.DatabaseName = "JIOKOE";
                info.UserID = "sa";
                info.Password = "k@r1bu";

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();


                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "TrialBalance");
                return Json(new { result = 0, err = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        [Authorize]
        public ActionResult BalanceSheet(string endDate, string branch_id)
        {
            try
            {


                endDate = Convert.ToDateTime(endDate).ToShortDateString();

                ReportDocument rd = new ReportDocument();
                rd.Load(Request.MapPath(Request.ApplicationPath) + @"Reports\BalanceSheet.rpt");



                rd.SetParameterValue("@endDate", endDate);
                rd.SetParameterValue("@branch_id", branch_id);

                ConnectionInfo info = new ConnectionInfo();
                info.DatabaseName = "JIOKOE";
                info.UserID = "sa";
                info.Password = "k@r1bu";

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();


                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "BalanceSheet");
                return Json(new { result = 0, err = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        } 
        [Authorize]
        public ActionResult IncomeStatement(string endDate, string branch_id)
        {
            try
            {


                endDate = Convert.ToDateTime(endDate).ToShortDateString();

                ReportDocument rd = new ReportDocument();
                rd.Load(Request.MapPath(Request.ApplicationPath) + @"Reports\IncomeStatement.rpt");



                rd.SetParameterValue("@endDate", endDate);
                rd.SetParameterValue("@branch_id", branch_id);



                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();


                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "InconeStatement");
                return Json(new { result = 0, err = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        


    }
}
