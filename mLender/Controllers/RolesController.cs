﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [AccessDeniedAuthorize(Roles = "Manager,Admin")]

    public class RolesController : Controller
    {
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();

        // GET: Roles
        [AccessDeniedAuthorize(Roles ="Admin,Super Admin,Manager")]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
               

                if (!isAdminUser())
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Roles");
            }

            var Roles = JiokoeDb.Roles.ToList();
            return View(Roles);
        }
        public Boolean isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;

                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(JiokoeDb));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "Admin"|| s[0].ToString()=="Super Admin"|| s[0].ToString()=="Manager")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        [AccessDeniedAuthorizeAttribute(Roles ="Admin,Super Admin,Manager")]
        
        public ActionResult Create()
        {
            if (User.Identity.IsAuthenticated)
            {


                if (!isAdminUser())
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Create", "Roles");
            }

            var Role = new IdentityRole();
            return View(Role);
        }

        /// <summary>
        /// Create a New Role
        /// </summary>
        /// <param name="Role"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(IdentityRole Role)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!isAdminUser())
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Create", "Roles");
            }

            JiokoeDb.Roles.Add(Role);
            JiokoeDb.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}
