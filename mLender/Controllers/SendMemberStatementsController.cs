﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    public class SendMemberStatementsController : Controller
    {
        // GET: SendMemberStatements
        EmailStatementsMonthly statementsMonthly = new EmailStatementsMonthly();
        string loan_no = "";
        DateTime fromDate;
        DateTime toDate;
        string memberName = "";
        string memberEmail = "";
        public ActionResult EmailStatement()
        {
            try
            {
                SendMailsWithAttachments sendMails = new SendMailsWithAttachments();

                var mails = sendMails.BulkEmail(memberEmail);
                return Json(mails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public ActionResult EmailLoanStatementPerLoan()
        {
            string LoanstatementPdfPaths = Request.MapPath(Request.ApplicationPath) + @"LoanStatements\";


            //Check if file exists with its full path
            DirectoryInfo directory = new DirectoryInfo(LoanstatementPdfPaths);
            try
            {
                if (statementsMonthly.checkIfItsEndOfMonth())//check if it is endmonth so you can send the mail
                {
                    GetEmailsAndLoanNoForMailing getEmails = new GetEmailsAndLoanNoForMailing();
                    var data = getEmails.Getdata();



                    foreach (var item in data)
                    {
                        loan_no = item.loan_number;
                        memberEmail = item.email_address;
                        memberName = item.Name;
                        fromDate = item.application_date;
                        toDate = item.LastMonthDay;

                        

                        
                       //delete files if exists 
                        foreach (FileInfo file in directory.GetFiles())
                        {
                            file.Delete();
                        }

                        ReportDocument rd = new ReportDocument();
                        rd.Load(Request.MapPath(Request.ApplicationPath) + @"Reports\LoanStatementPerLoan.rpt");



                        rd.SetParameterValue("@loan_no", loan_no);
                        rd.SetParameterValue("@fromDate", fromDate);
                        rd.SetParameterValue("@toDate", toDate);

                        ConnectionInfo info = new ConnectionInfo();
                        info.DatabaseName = "JIOKOE";
                        info.UserID = "sa";
                        info.Password = "k@r1bu";

                        Response.Buffer = false;
                        Response.ClearContent();
                        Response.ClearHeaders();


                        Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);//export as PDF                                                                    


                        rd.ExportToDisk(ExportFormatType.PortableDocFormat, LoanstatementPdfPaths + loan_no + ".pdf");//export to disk then send email

                        SendMailsWithAttachments sendMails = new SendMailsWithAttachments();

                        var mails = sendMails.BulkEmail(memberEmail);

                    }

                    return View();

                }

                return View();

            }


            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            
        }
    }
}