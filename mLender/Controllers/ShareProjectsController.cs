﻿using Dapper;
using Microsoft.AspNet.Identity;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    public class ShareProjectsController : Controller
    {

        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();
        bool New = true;
        string code = "";
        [Authorize]

        public ActionResult Index()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var project_list = sqlConnection.Query<Shareproject>
                        ("select * from shareproject where IsDeleted=0").ToList();
                    return View(project_list);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Error! " + ex.Message;
                return RedirectToAction("index");

            }
        }

        //
        // GET: /ShareProjects/Details/5
        [Authorize]

        public ActionResult Details(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var share_details = sqlConnection.Query<Shareproject>
                        ("get_shareproject_per_ID", new { id }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    if (share_details == null)
                    {
                        return new HttpStatusCodeResult(404, "Project of Code:" + '\'' + id + '\'' + " does not exists!");
                    }
                    else
                    {

                        //return as true or false not 0 or 1
                        var check_off = sqlConnection.Query<Shareproject>
                            ("checkifCheckIsAllowed", new { id }, commandType: CommandType.StoredProcedure).Select(c => c.allow_check_off).SingleOrDefault().ToString();
                        ViewBag.check_off = check_off;

                        //Check if refundable or not
                        var isrefundable = sqlConnection.Query<Shareproject>
                            ("checkIfShareTypeIsRefundable", new { id }, commandType: CommandType.StoredProcedure).
                            Select(r => r.refundable).SingleOrDefault().ToString();
                        ViewBag.isrefundable = isrefundable;

                        //Get ledger name
                        var ledger = sqlConnection.Query<Shareproject>
                           ("checkLedgerForProjectType", new { id }, commandType: CommandType.StoredProcedure).
                           Select(r => r.ledger).SingleOrDefault().ToString();
                        ViewBag.ledger = ledger;

                        return View(share_details);
                    }
                }
            }
            catch (Exception ex)
            {

                TempData["Error"] = "Error! " + ex.Message;
                return RedirectToAction("index");
            }
        }

        //
        // GET: /ShareProjects/Create
        [Authorize]

        public ActionResult Create()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    List<Ledger> project_ledger = sqlConnection.Query<Ledger>
                        ("get_project_ledger", commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.project_ledger = new SelectList(project_ledger,"code","name");

                    return View("Create");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Error! " + ex.Message;
                return RedirectToAction("index");
            }
        }

        //
        // POST: /ShareProjects/Create

        [HttpPost]
        [Authorize]

        public JsonResult Create(Shareproject model)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                     Shareproject shareproject = new Shareproject();

                    var system_user = HttpContext.User.Identity.GetUserName();
                    shareproject.name = model.name;
                    shareproject.code = model.code;
                    shareproject.minimum_amount =model.minimum_amount;
                    shareproject.maximum_amount =model.maximum_amount;
                    shareproject.ledger = model.ledger;
                    shareproject.refundable =model.refundable;
                    shareproject.allow_check_off =model.allow_check_off;
                    shareproject.project_type = model.project_type;
                    shareproject.audit_id = system_user;
                    shareproject.audit_time = DateTime.Now;

                    

                    //insert to db
                    var affected_rows = sqlConnection.Execute("saveshareproject",
                        new
                        {
                            shareproject.code,
                            shareproject.name,
                            shareproject.minimum_amount,
                            shareproject.maximum_amount,
                            shareproject.ledger,
                            shareproject.refundable,
                            shareproject.allow_check_off,
                            shareproject.project_type,
                            shareproject.audit_id,
                            shareproject.audit_time,
                            New
                        }, commandType: CommandType.StoredProcedure);

                    TempData["Success"] = "Details saved successfully!";
                    return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Error! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);

            }
        }

        //
        // GET: /ShareProjects/Edit/5
        [Authorize]

        public ActionResult Edit(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    List<Ledger> project_ledger = sqlConnection.Query<Ledger>
                        ("get_project_ledger", commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.project_ledger = new SelectList(project_ledger, "code", "name");

                    //check if there's data to edit
                    var shareproject = sqlConnection.Query<Shareproject>
                       ("get_share_project_Edit", new { id }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                   
                        return View(shareproject);


                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Error! " + ex.Message;
                return RedirectToAction("index");
            }
        }

        //
        // POST: /ShareProjects/Edit/5

        [HttpPost]
        [Authorize]

        public ActionResult Edit(string id, Shareproject model)
        {
            try
            {
                New = false;
                
                return Create(model);

            }
            catch (Exception ex)
            {
                TempData["Error"] = "Error! " + ex.Message;
                return RedirectToAction("index");
            }
        }

        //
        // GET: /ShareProjects/Delete/5
        [Authorize]

        public ActionResult Delete(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    var get_project = sqlConnection.Query<Shareproject>
                        ("select * from shareproject where IsDeleted=0 and code=@id",
                        new { @id = id }, commandType: CommandType.Text).SingleOrDefault();
                    return View(get_project);
                }
            }
            catch (Exception ex)
            {

                TempData["Error"] = "Error! " + ex.Message;
                return RedirectToAction("index");
            }
        }

        //
        // POST: /ShareProjects/Delete/5

        [HttpPost]
        [Authorize]

        public ActionResult Delete(string id, FormCollection collection)
        {
            try
            {
                using (sqlConnection)
                {
                    //check record if existing
                    var shareproject = sqlConnection.Query<Shareproject>
                        ("select code from shareproject where code=@id", new { id }, commandType: CommandType.Text).SingleOrDefault();
                    if (shareproject == null)
                    {
                        return new HttpStatusCodeResult(404, "No record found for that code!");
                    }
                    else
                    {
                        var delete = sqlConnection.Execute
                        ("update shareproject set Isdeleted=1 where code=@id", new { id },
                        commandType: CommandType.Text);

                        TempData["Success"] = "Details deleted successfully!";

                        return RedirectToAction("Index");
                    }

                }

            }
            catch (Exception ex)
            {

                TempData["Error"] = "Error! " + ex.Message;
                return RedirectToAction("index");
            }
        }
    }
}
