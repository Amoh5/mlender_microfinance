﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    public class UploadsController : Controller
    {

        private static string ConnectionString =
           ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();


        DateTime audit_time = DateTime.Now;
        
        [Authorize]
        [HttpGet]
        public ActionResult member()
        {
            try
            {
                using (sqlConnection)
                {
                    return View();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        [Authorize]
        [HttpPost]
        public ActionResult member(string file)
        {

            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();
                    var dt = new DataTable();

                    dt.Columns.Add("Member_number");
                    dt.Columns.Add("Name");

                    for (int i = 0; i < 10; i++)
                    {
                        dt.Rows.Add(i + 1, "Member_number" + i + 1);
                        dt.Rows.Add(i + 1, "Name" + i + 1);

                    }


                    using (var sqlBulk=new SqlBulkCopy(sqlConnection))
                    {
                        sqlBulk.DestinationTableName = "tempMemberdetails";
                        sqlBulk.WriteToServer(dt);
                    }
                    return View();

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
    }
}