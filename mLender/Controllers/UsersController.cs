﻿using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();
        private static string ConnectionString =
           ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        // GET: Users
        public ActionResult Index()
        {

            
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;



                ViewBag.displayMenu = "No";

                if (isAdminUser())
                {
                    ViewBag.displayMenu = "Yes";
                }
                //var Users = JiokoeDb.Users.ToList();
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    List<UserWithRole> userWithRoles = sqlConnection.Query<UserWithRole>("getUserWithRole").ToList();

                    return View(userWithRoles);

                }


            }
            else
            {
                ViewBag.Name = "Not Logged IN";
            }
            return View();
        }
        public Boolean isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;

                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(JiokoeDb));
                var s = UserManager.GetRoles(user.GetUserId());
                    if (s[0].ToString() == "Admin" || s[0].ToString() == "Super Admin" || s[0].ToString() == "Manager")

                    {
                        return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}