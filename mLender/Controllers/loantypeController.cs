﻿using Dapper;
using mLender.Models;
using mLender.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Controllers
{
    [AccessDeniedAuthorize(Roles = "Manager,Admin")]

    public class LoantypeController : Controller
    {
        //
        // GET: /loantype/
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private LoanType loantype = new LoanType();
        private LoanTypeRepository loanTypeRepository = new LoanTypeRepository();

        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();
        bool IsNew = true;
        string loan_code = "";

        [Authorize]

        public ActionResult Index()
        {

            try
            {
                List<LoanType> model = new List<LoanType>();

                var list = loanTypeRepository.getLoantypes(model);
                return View(list);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /loantype/Details/5
        [Authorize]

        public ActionResult Details(string id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var loan_product = sqlConnection.Query<LoanType>("getLoanType_Per_ID",
                        new { @code = id }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    return View(loan_product);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // GET: /loantype/Create
        [Authorize]

        public ActionResult Create()
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    //populate interest ledger account..get revenue group
                    IEnumerable<Ledger> revenue_account =
                        sqlConnection.Query<Ledger>("select l.code code, l.name  from ledger l join " +
                        "ledger_group lg on lg.code = l.ledger_group and lg.class_code =@code ", new { code = 4000 }, commandType: CommandType.Text);
                    ViewBag.interest_account = new SelectList(revenue_account, "code", "name");

                    //populate loan principal ledger--get assets
                    IEnumerable<Ledger> loan_account =
                        sqlConnection.Query<Ledger>("select l.code code, l.name  from ledger l join " +
                        "ledger_group lg on lg.code = l.ledger_group and lg.class_code =@code ", new { code = 1000 }, commandType: CommandType.Text);
                    ViewBag.principal_account = new SelectList(loan_account, "code", "name");

                    //populate loan repayment methods in dropdownlist 
                    IEnumerable<LoanrepaymentMethod> repayment_method =
                        sqlConnection.Query<LoanrepaymentMethod>("select Code,Name from LoanRepaymentMethods", commandType: CommandType.Text);

                    ViewBag.repayment_method = new SelectList(repayment_method, "Code", "Name");


                    return View();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /loantype/Create

        [HttpPost]
        [Authorize]

        public JsonResult Create(LoanType model)
        {
            try
            {
                var insertLoanType=loanTypeRepository.AddLoanType(model,true);

                TempData["Success"] = "Details saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                TempData["Error"] = "Details not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /loantype/Edit/5
        [Authorize]

        public ActionResult Edit(string id)
        {
            try
            {
                using (sqlConnection)
                {
                        sqlConnection.Open();

                    //populate interest ledger account..get revenue group
                    List<Ledger> revenue_account =
                        sqlConnection.Query<Ledger>("getInterestAccount", commandType: CommandType.StoredProcedure).ToList();
                    ViewBag.interest_account = new SelectList(revenue_account, "Code", "Name");

                    //populate loan principal ledger--get assets
                    List<Ledger> loan_account =
                        sqlConnection.Query<Ledger>("getPrincipalAccount", commandType: CommandType.StoredProcedure).ToList();
                    ViewBag.principal_account = new SelectList(loan_account, "Code", "Name");

                    //populate loan repayment methods in dropdownlist 
                    List<LoanrepaymentMethod> repayment_method =
                        sqlConnection.Query<LoanrepaymentMethod>("getRepaymentMethods", commandType: CommandType.StoredProcedure).ToList();

                    ViewBag.repayment_method = new SelectList(repayment_method, "Code", "Name");



                    loantype = sqlConnection.Query<LoanType>
                        ("getLoanType_Per_ID", new { code = id }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    return View(loantype);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        //
        // POST: /loantype/Edit/5

        [HttpPost]
        [Authorize]

        public ActionResult Edit(string id, LoanType model)
        {
            try
            {
                var updateLoanType = loanTypeRepository.AddLoanType(model,false);

                TempData["Success"] = "Changes saved succesfully";

                return Json(TempData["Success"].ToString(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                TempData["Error"] = "Changes not saved! " + ex.Message;
                return Json(TempData["Error"].ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /loantype/Delete/5
        [Authorize]

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /loantype/Delete/5

        [HttpPost]
        [Authorize]

        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
