﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class ApproveLoans
    {
        public string member_number { get; set; }
        public string loan_no { get; set; }
        public DateTime appraisal_date { get; set; }

        public string loan_name { get; set; }

        public decimal Deposit { get; set; }
        public decimal applied_amount { get; set; }
        public decimal guaranteed_amount { get; set; }
        public decimal period { get; set; }
        public decimal approved_period { get; set; }

        public decimal approved_amount { get; set; }

    }
}