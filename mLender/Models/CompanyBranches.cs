﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Models
{
    public class CompanyBranches
    {

        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        CompanyOfficer Officer = new CompanyOfficer();
        public string branch_id { get; set; }
        [Required]
        public string branch_name { get; set; }
        public string officer_name { get; set; }
        public string location { get; set; }

        public string telephone { get; set; }
        [DataType(DataType.EmailAddress)]
        public string email_address { get; set; }

        //public bool is_active { get; set; }

        public bool allow_check_off { get; set; }

        public string control_account { get; set; }

        public decimal minimum_contribution { get; set; }

        public decimal minimum_shares { get; set; }
        public decimal registration_fee { get; set; }

        public decimal rejoining_fee { get; set; }

        public int check_off_date { get; set; }

    }

}