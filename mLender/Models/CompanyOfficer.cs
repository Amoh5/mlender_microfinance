﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class CompanyOfficer
    {

        public string officer_id { get; set; }
        public string first_name { get; set; }
        public string other_name { get; set; }
        public string telephone { get; set; }
        public string email_address { get; set; }
        public string id_no { get; set; }
        public string audit_id { get; set; }
        public DateTime audit_time { get; set; }
        public string fullname { get; set; }
        
            

    }

}
