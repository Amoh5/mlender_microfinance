﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class Deposits
    {
        public string member_numbr { get; set; }
        public DateTime contribution_date { get; set; }
        public string description { get; set; }
        public string reference { get; set; }
        public decimal amount { get; set; }
        public string payment_mode { get; set; }




    }
}