﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class DueLoans
    {
        public string MemberNo { get; set; }
        public string loan_number { get; set; }
        public string Name { get; set; }
        public DateTime application_date { get; set; }
        public string loan_code { get; set; }
        public decimal applied_amount { get; set; }
        public decimal repay_period { get; set; }
        public decimal   Balance { get; set; }
        public DateTime   DueDate { get; set; }

    }
}