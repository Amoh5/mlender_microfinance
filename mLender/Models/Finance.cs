﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class Finance
    {
       
    }
    public class Cashbook
    {
        public string ledger { get; set; }
        public DateTime transaction_date { get; set; }
        public string transaction_narration { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal balance { get; set; }
               

    }
    public class trialBalance
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

        public string ledger { get; set; }

        public decimal Debit { get; set; }
        public decimal Credit { get; set; }


    } 
    public class balancesheet
    {
        public DateTime endDate { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public string Branch { get; set; }


    }

}