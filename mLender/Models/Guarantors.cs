﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class Guarantors
    {
        public string loan_number { get; set; }
        public string guarantor_number { get; set; }
        public string Name { get; set; }

        public decimal amount_guaranteed { get; set; }
        public string audit_id { get; set; }
        public DateTime date_added { get; set; }

    }
}