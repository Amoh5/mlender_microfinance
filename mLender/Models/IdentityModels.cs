﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace mLender.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        //New properties are added
        public string FirstName { get; set; }

        public string OtherName { get; set; }
        public string IDNo { get; set; }
        public string Telephone { get; set; }
        public string ResetPasswordCode { get; internal set; }
    }

    public class JiokoeDbContext : IdentityDbContext<ApplicationUser>
    {
        public JiokoeDbContext()
            : base("JIOKOE", throwIfV1Schema: false)
        {
        }

        public static JiokoeDbContext Create()
        {
            return new JiokoeDbContext();
        }
    }
    
}