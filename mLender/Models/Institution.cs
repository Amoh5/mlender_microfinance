﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class Institution
    {
        public string name { get; set; }
        public string address { get; set; }
        public string telephone { get; set; }
        public string email_address { get; set; }
        public string tag_line { get; set; }
        public string Logo { get; set; }
        public bool Is_Matatu_Sacco { get; set; }

    }
}