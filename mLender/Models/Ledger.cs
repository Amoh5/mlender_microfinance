﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace mLender.Models
{
    public class Ledger
    {


        public string Class { get; set; }
        public string Ledger_group { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Allow_Overdraft { get; set; }
        public bool IsBank { get; set; }



    }
}