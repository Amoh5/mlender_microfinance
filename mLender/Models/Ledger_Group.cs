﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class Ledger_Group
    {
        public string code { get; set; }
        public string class_code { get; set; }

        public string account_no_prefix { get; set; }

        public string group_name { get; set; }

        public bool iscash_book { get; set; }


    }
}