﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class LoanApplication
    {
        public string loan_number { get; set; }
        [Required]
        public string member_number { get; set; }
        public DateTime application_date { get; set; }
        public string loan_code { get; set; }
        public decimal applied_amount { get; set; }
        public decimal repay_period { get; set; }
        public string purpose { get; set; }
        public string guarantor { get; set; }
        public decimal amount_guaranteed { get; set; }
        public string insurer { get; set; }
        public bool insured { get; set; }
        public decimal basic_salary { get; set; }
        public decimal house_allowance { get; set; }
        public decimal other_allowance { get; set; }
        public decimal other_payment { get; set; }
        public decimal total_deduction { get; set; }
        public string audit_id { get; set; }
        public DateTime audit_time { get; set; }
        public string loan_status { get; set; }
        public string branch { get; set; }
        public decimal paid_amount { get; set; }

    }
    public class ApproveLoan
    {
        public string member_number { get; set; }
        public string fullname { get; set; }

        public string loan_no { get; set; }
        public DateTime application_date { get; set; }

        public string loan_name { get; set; }

        public decimal Deposit { get; set; }
        public decimal applied_amount { get; set; }
        public decimal amount_guaranteed { get; set; }
        public decimal period { get; set; }

        public decimal approved_period { get; set; }

        public decimal approved_amount { get; set; }
        public Boolean deduct_oustanding_loan { get; set; }
        public DateTime appraisal_date { get; set; }

    }
    public class EffectLoan
    {
        public string loan_no { get; set; }

        public string member_number { get; set; }
        public string fullname { get; set; }

        public DateTime appraisal_date { get; set; }
        public DateTime effect_date { get; set; }

        public decimal approved_amount { get; set; }
        public decimal net_amount { get; set; }
        public decimal approved_period { get; set; }
        public decimal processing_fee { get; set; }
        public decimal insurance_premium { get; set; }
        public decimal other_loan_charges { get; set; }
        public string loan_to_clear { get; set; }
        public string voucherNo { get; set; }
        public string credit_account { get; set; }

                                 
    }

    public class LoanDefaulters
    {
        public string loanno { get; set; }
        public string fullname { get; set; }
        public DateTime DateIssued { get; set; }
        public decimal Amount { get; set; }
        public decimal Period { get; set; }
        public decimal defaultedPeriod { get; set; }
        public decimal Paid { get; set; }
        public DateTime LastDate { get; set; }
        public decimal Balance { get; set; }
    }
}