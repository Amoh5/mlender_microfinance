﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class LoanCollateral
    {
        public string loan_number { get; set; }
        public string code { get; set; }
        public string Owner { get; set; }
        public string OwnerIDNo { get; set; }
        public string CategoryCode { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public decimal CollateralValue { get; set; }
        public decimal AmountGuaranteed { get; set; }
        //public decimal AmountSecured { get; set; }

    }
}