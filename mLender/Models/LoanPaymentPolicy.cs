﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class RegFeePolicy
    {
        public string credit_account { get; set; }
        public bool mandatory_on_registration { get; set; }
        public bool recover_from_deposit { get; set; }
        public bool deduct_from_loan { get; set; }
        public bool recover_from_checkoff { get; set; }

    }
    public class LoanInsurancePolicy
    {
        public string controll_account { get; set; }
        public bool deduct_from_loan { get; set; }
        public bool recover_from_checkoff { get; set; }

        public bool Percentage { get; set; }

    }
}