﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class LoanType
    {
        [StringLength(3, ErrorMessage = "Name must not be equal to {1} char", MinimumLength = 3)]
        public string code { get; set; }
        [StringLength(20, ErrorMessage = "Name must not be more than {1} and less than {2} char", MinimumLength = 4)]
        public string name { get; set; }
        public decimal maximum_amount { get; set; }
        public decimal minimum_amount { get; set; }
        public decimal interest { get; set; }
        public string repayment_method { get; set; }
        public decimal loan_deposit_ratio { get; set; }
        public bool deduct_full_interest_first_month { get; set; }
        public bool insurable { get; set; }

        public bool deduct_premium_on_first_month { get; set; }

        public bool calculate_premium_on_loan_balance { get; set; }

        public bool ignore_3rd_rule { get; set; }

        public int maximum_period { get; set; }

        public int minimum_giarantors { get; set; }

        public bool trustee_loan { get; set; }

        public int membership_period { get; set; }
        public string interest_account { get; set; }
        public string principal_account { get; set; }

        public bool allow_check_off { get; set; }

        public string penalty_account { get; set; }
        public decimal penalty_amount { get; set; }
        public bool is_Penalty_Percentage { get; set; }
        public string processing_fee_account { get; set; }
        public decimal processing_fee_amount { get; set; }
        public bool is_Processing_fee_percentage { get; set; }
        public string appraisal_fee_account { get; set; }
        public bool is_appraisal_fee_percentage { get; set; }
        public decimal appraisal_fee_amount { get; set; }










    }
}