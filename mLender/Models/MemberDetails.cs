﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Models
{
    public class MembersDetails
    {
        public string member_number { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [StringLength(10, ErrorMessage = "First Name cannot be more than {1} and less than {2}", MinimumLength = 4)]
        public string first_name { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [StringLength(20, ErrorMessage = "Other Name cannot be more than {1} and less than {2}", MinimumLength = 4)]

        public string other_name { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string gender { get; set; }
        [Required]
        public DateTime date_of_birth { get; set; }
        [Required]
        public DateTime date_of_registration { get; set; }

        [DataType(DataType.Text)]
        public string residence { get; set; }

        [DataType(DataType.Text)]
        public string occupation { get; set; }
        [Required]

        [DataType(DataType.Text)]
        //public string branch_id { get; set; }

        public string branch_id { get; set; }



        [Required]

        [DataType(DataType.Text)]
        public string idno { get; set; }
        [Required]

        [DataType(DataType.PhoneNumber)]
        public string telephone { get; set; }
        [Required]

        [DataType(DataType.EmailAddress)]
        public string email_address { get; set; }
        [Required]

        [DataType(DataType.Text)]
        public string marital_status { get; set; }

        [DataType(DataType.ImageUrl)]
        //public string photo
        //{
        //    get;
        //    set;
        //}
        //[FileSize(10240)]
        //[FileTypes("jpg,jpeg,png")]
        public string photo { get; set; }
        public bool is_active { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:N2}")]

        public decimal Monthly_contribution { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:N2}")]

        public decimal Monthly_savings { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal Monthly_welfare { get; set; }
        [DataType(DataType.Text)]
        public string payroll_no { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Classification { get; set; }
        public bool IsStaff { get; set; }
        public string fullname { get; set; }
        public string officer_id { get; set; }
        public string Referee { get; set; }


    }
}