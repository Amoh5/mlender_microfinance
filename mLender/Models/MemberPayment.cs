﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class MemberPayment
    {
        public DateTime payment_date { get; set; }
        public string voucher_no { get; set; }
        public string member_number { get; set; }
        public string bank_account { get; set; }
        public decimal amount { get; set; }
        public string payment_mode { get; set; }
        public string reference_no { get; set; }
        public string category { get; set; }




    }

    public class MemberRefund
    {
        public DateTime RefundDate { get; set; }
        public string MemberNumber { get; set; }
        public string RefundType { get; set; }
        public string RefundNumber { get; set; }
        public decimal Amount { get; set; }
        public string Narration { get; set; }
        public string PaymentMode { get; set; }

        public string ReferenceNo { get; set; }
        public string ChargeAccount { get; set; }
        public string AudtID { get; set; }
        public DateTime AuditTime { get; set; }

    }
    public class RefundTypes
    {
        public string RefundType { get; set; }
        public string Code { get; set; }

    } 
    public class otherPayments
    {
        public DateTime payment_date { get; set; }
        public string voucher_no { get; set; }
        public string member_number { get; set; }   
        public string bank_account { get; set; }//credit account
        public string debit_account { get; set; }//debit account/expenses
        public decimal amount { get; set; }
        public string payment_mode { get; set; }
        public string reference_no { get; set; }
        public string category { get; set; }
        public string narration { get; set; }

    }
}