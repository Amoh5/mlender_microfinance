﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class MemberStatementReport
    {
        public string member_number { get; set; }
        public string fullname { get; set; }

        public DateTime startDate { get; set; }

        public DateTime endDate { get; set; }

    }
}