﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Models
{
    public class MiscellaneousSettings
    {
        public string Name { get; set; }
        public string Account_No { get; set; }
        public bool Percentage { get; set; }

        public decimal default_amount { get; set; }
        public string type { get; set; }

    }

}