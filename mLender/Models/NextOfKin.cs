﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class NextOfKin
    {
        public string kin_no { get; set; }
        public string member_number { get; set; }
        public string kin_name { get; set; }
        public string relationship { get; set; }
        public decimal percentage { get; set; }
        public bool is_active { get; set; }
        public DateTime date_added { get; set; }
        public DateTime date_modified { get; set; }
        public string username_id { get; set; }
        public string document_number { get; set; }



    }
}