﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class Receipt
    {
        public string type { get; set; }
        public string receipt_no { get; set; }
        public string member_number { get; set; }
        public string description { get; set; }
        public string loan_no { get; set; }
        public DateTime receipt_date { get; set; }
        public string debit_account { get; set; }
        public string reference_number { get; set; }
        public decimal amount { get; set; }

        //decimal item_amount = 0;
                     
    }
    public class NonMemberWalletReceipt
    {
        public string type { get; set; }
        public string receipt_no { get; set; }
        public string IDNo { get; set; }
        public string description { get; set; }
        public string reference_number { get; set; }
        public string MobileNo { get; set; }
        public DateTime receipt_date { get; set; }
        public string debit_account { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public string AuditId { get; set; }
        public DateTime AuditTime { get; set; }
    }
    public class OtherReceipts
    {
        public string type { get; set; }
        public string Name { get; set; }
        public string receipt_no { get; set; }
        public string description { get; set; }
        public string reference_number { get; set; }
        public DateTime receipt_date { get; set; }
        public string debit_account { get; set; }
        public string credit_account { get; set; }
        public decimal Amount { get; set; }
        public string AuditId { get; set; }
        public DateTime AuditTime { get; set; }
    }
}