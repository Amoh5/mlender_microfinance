﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class Shareproject
    {
        public string code { get; set; }
        public string name { get; set; }
        public decimal minimum_amount { get; set; }
        public decimal maximum_amount { get; set; }
        public string ledger { get; set; }
        public bool refundable { get; set; }
        public bool allow_check_off { get; set; }
        public string project_type { get; set; }
        public string audit_id { get; set; }
        public DateTime audit_time { get; set; }

    }
}