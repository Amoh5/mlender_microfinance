﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Models
{
    public class UserWithRole
    {
        
            public string UserName { get; set; } // You can alias the SQL output to give these better names
            public string Role { get; set; }
    }
}