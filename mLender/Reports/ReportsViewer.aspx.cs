﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mLender.Reports
{
    public partial class ReportsViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            var reportPath = Session["reportPath"].ToString();
            // attached our report to viewer and set database login.
            ReportDocument report = new ReportDocument();
            report.Load(reportPath);
            string ledger="1104";
            string branch_id="6";
            string startDate=Convert.ToDateTime("2020-01-01").ToShortDateString();
            string endDate= Convert.ToDateTime("2020-04-01").ToShortDateString();
            //report.SetParameterValue("@ledger", ledger);
            //report.SetParameterValue("@startDate", startDate);
            //report.SetParameterValue("@endDate", endDate);
            //report.SetParameterValue("@branch_id", branch_id);
           //// report.SetDataSource("EXEC generateCashbookTransactions "+ledger+","+startDate+","+endDate+","+branch_id+"");

            report.SetDatabaseLogon("sa", "k@r1bu", @"CHERUIYOT", "jiokoe");
            CrystalReportViewer1.ReportSource = report;
            CrystalReportViewer1.RefreshReport();
        }
    }
}