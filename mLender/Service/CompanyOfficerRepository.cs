﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace mLender.Service
{
    public class CompanyOfficerRepository : IcompanyOfficer
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private JiokoeDbContext JiokoeDb = new JiokoeDbContext();
        private CompanyOfficer officer = new CompanyOfficer();
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        

        public CompanyOfficer AddOfficer( CompanyOfficer companyOfficer, string id)
        {
         SqlConnection sqlConnection = new SqlConnection(ConnectionString);
          string AuditID = GetCurrentUserService.CurrentUser();

            try
            {

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var username = AuditID;
                    officer.officer_id = companyOfficer.officer_id;

                    officer.first_name = companyOfficer.first_name;

                    officer.other_name = companyOfficer.other_name;

                    officer.telephone = companyOfficer.telephone;

                    officer.email_address = companyOfficer.email_address;

                    officer.id_no = companyOfficer.id_no;
                    officer.audit_id = username;
                    officer.audit_time = DateTime.Now;
                    if (checkIfUnique(officer.id_no, officer.email_address, officer.telephone))
                    {


                        var insertOfficer = sqlConnection.Execute("saveofficerdetails", new
                        {
                            officer.officer_id,
                            officer.first_name,
                            officer.other_name,
                            officer.telephone,
                            officer.email_address,
                            officer.id_no,
                            officer.audit_id,
                            officer.audit_time
                        }, commandType: CommandType.StoredProcedure);
                    }
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return companyOfficer;
        }

        public int DeleteOfficer(string id)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(ConnectionString);
                string AuditID = GetCurrentUserService.CurrentUser();
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    CompanyOfficerRepository officerRepository = new CompanyOfficerRepository();


                    if (!officerRepository.HasClients(id))
                    {
                        var officer_id = id;
                        var delete = sqlConnection.Execute("deletCompanyOfficer", new { officer_id },
                            commandType: CommandType.StoredProcedure);
                        return delete;
                    }
                    else
                    {
                        throw new
                            ArgumentException("This officer has clients,therefore cannot be deleted, assign clients to other officers first");                           
                    }

                    

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public CompanyOfficer EditOfficer(CompanyOfficer officer, string id)
        {
            try
            {
                return AddOfficer(officer, id);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public bool HasClients(string id)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(ConnectionString);

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var clients = sqlConnection.Query("checkOfficerClients", new { officer_id=id },
                        commandType: CommandType.StoredProcedure).Count();
                    if (clients>0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public bool checkIfUnique(string IDno, string email, string Telephone)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(ConnectionString);

                using (sqlConnection)
                {

                    var memberData = sqlConnection.Query("checkUniqueAgentData", //check count if greater than zero then not unique
                        new { IDno, email, Telephone }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    //we use int since we arechecking their counts
                    int id = memberData.IDNo;
                    int mail = memberData.Email;
                    int MobileNo = memberData.Telephone;

                    if (id > 0)
                    {
                        throw new ArgumentException("Similar IDNo exists in database");
                    }
                    else if (mail > 0)
                    {
                        throw new ArgumentException("Similar email address exists in database");

                    }
                    else if (MobileNo > 0)
                    {
                        throw new ArgumentException("Similar email address exists in database");

                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                oErrorLog oErrorLog = new oErrorLog();

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
            finally
            {
                sqlConnection.Dispose();
                sqlConnection.Close();
            }
        }
    }
}