﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using mLender.Models;

namespace mLender.Service
{
    public class DepositRepository : IDeposit
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;

        oErrorLog oErrorLog = new oErrorLog();
        string AuditID = GetCurrentUserService.CurrentUser();
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        decimal deposit;
        public Deposits AddDeposit(Deposits deposits)
        {
            throw new NotImplementedException();
        }

        public decimal CheckDepositBalance(string member_number)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var summarybalances = sqlConnection.Query("getMember_summary_Info",
                        new { member_number }, commandType: CommandType.StoredProcedure).First();

                    deposit = summarybalances.Deposit;


                    return deposit;
                }
            }
            catch (Exception ex )
            {

                throw new Exception(ex.Message);
            }
        }

        public List<Deposits> list()
        {
            throw new NotImplementedException();
        }
    }
}