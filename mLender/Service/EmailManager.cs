﻿using Microsoft.AspNet.Identity;
using SendGrid.Helpers.Mail;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mail;
using System;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using System.Linq;

namespace mLender.Service
{
    public class EmailManager
    {

        private static string ConnectionString =
                ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;

        public static void AppSettings(out string UserID, out string Password, out string SMTPPort, out string Host,out string fromAddress)
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);

            //UserID = ConfigurationManager.AppSettings.Get("UserID");
            //Password = ConfigurationManager.AppSettings.Get("Password");
            //SMTPPort = ConfigurationManager.AppSettings.Get("SMTPPort");
            //Host = ConfigurationManager.AppSettings.Get("Host");
            using (sqlConnection)
            {
                sqlConnection.Open();
                var emailSettings = sqlConnection.Query("SELECT * FROM EmailSettings", commandType: CommandType.Text).SingleOrDefault();
                 Host = emailSettings.smtpserver;
                SMTPPort = emailSettings.smtpport;
                UserID = emailSettings.smtpuser;
                Password = emailSettings.smtppassword;
                var ssl = emailSettings.SSL;
                fromAddress = emailSettings.FromAddress;
                var subject = emailSettings.Subject;
                var Body = emailSettings.Body;
            }
        }
        public static void SendEmail(string From, string Subject, string Body, string To, string UserID, string Password, string SMTPPort, string Host)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.To.Add(To);
            mail.From = new MailAddress(From);
            mail.Subject = Subject;
            mail.Body = Body;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = Host;
            smtp.Port = Convert.ToInt16(SMTPPort);
            smtp.Credentials = new NetworkCredential(UserID, Password);
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    }
}
