﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace mLender.Service
{
    public class EmailStatementsMonthly
    {
        private static string ConnectionString =
               ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        public  bool checkIfItsEndOfMonth()
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);
            bool IsEndMonth = false;

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var endMonth = sqlConnection.Query("checkIfEndOfMonth", commandType: CommandType.StoredProcedure).SingleOrDefault();
                    IsEndMonth = Convert.ToBoolean(endMonth.IsEndOfMonth);

                    if (IsEndMonth)
                    {
                        return IsEndMonth;
                    }
                    else
                    {
                        return IsEndMonth;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message) ;
            }
            finally
            {
                sqlConnection.Dispose();
                sqlConnection.Close();
            }

            

        }
    }
}