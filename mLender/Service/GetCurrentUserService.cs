﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mLender.Service
{
    public static class GetCurrentUserService
    {
        public static string CurrentUser()
        {
            string  _userId= System.Web.HttpContext.Current.User.Identity.GetUserName();

            return _userId;
        }
    }
}