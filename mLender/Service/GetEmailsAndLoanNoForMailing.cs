﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace mLender.Service
{
    public class GetEmailsAndLoanNoForMailing
    {
		private static string ConnectionString =
				ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
		public List<dynamic> Getdata()
        {
			SqlConnection sqlConnection = new SqlConnection(ConnectionString);

			try
			{
				using (sqlConnection)
				{
					sqlConnection.Open();
					var memberData = sqlConnection.Query("getDueLoans", commandType:CommandType.StoredProcedure).ToList();
					return memberData;
				}
			}
			catch (Exception ex)
			{

				throw new Exception(ex.Message);
			}
        }
    }
}