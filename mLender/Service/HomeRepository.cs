﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mLender.Service
{
    [Authorize]
    public class HomeRepository : IHome
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;

        oErrorLog oErrorLog = new oErrorLog();
        string AuditID = GetCurrentUserService.CurrentUser();
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        public IEnumerable<LoanApplication> getLoanGraphDataAnalysis(string startDate,string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    if (startDate == null || toDate == null || startDate == "" || toDate == "")
                    {
                        var loan_analysis = sqlConnection.Query<LoanApplication>("getLoanIssuedAnalysis", new { startDate, toDate }, commandType: CommandType.StoredProcedure);

                        return loan_analysis.ToList();
                    }
                    else
                    {

                        startDate = Convert.ToDateTime(startDate).ToShortDateString();
                        toDate = Convert.ToDateTime(toDate).ToShortDateString();
                        var loan_analysis = sqlConnection.Query<LoanApplication>("getLoanIssuedAnalysis", new { startDate, toDate }, commandType: CommandType.StoredProcedure);

                        return loan_analysis.ToList();
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<MembersDetails> getMemberGraphDataAnalysis(string startDate, string toDate)
        {
            //throw new NotImplementedException();
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    if (startDate==null||toDate==null || startDate == "" || toDate == "")
                    {
                        var data = sqlConnection.Query<MembersDetails>("getMemberGenderAnalysis", new { startDate, toDate },
                        commandType: CommandType.StoredProcedure);
                        return data.ToList();
                    }
                    else
                    {
                        startDate = Convert.ToDateTime(startDate).ToShortDateString();
                        toDate = Convert.ToDateTime(toDate).ToShortDateString();
                        var data = sqlConnection.Query<MembersDetails>("getMemberGenderAnalysis", new { startDate, toDate },
                            commandType: CommandType.StoredProcedure);


                        return data.ToList();
                    }
                    
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public IEnumerable<Shareproject> getShareprojectGraphDataAnalysis(string startDate, string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    if (startDate == null || toDate == null || startDate == "" || toDate == "")
                    {
                        var share_project_analysis = sqlConnection.Query<Shareproject>("getShareProjectAnalysis", new { startDate, toDate },
                        commandType: CommandType.StoredProcedure);

                        return share_project_analysis.ToList();
                    }
                    else
                    {
                        startDate = Convert.ToDateTime(startDate).ToShortDateString();
                        toDate = Convert.ToDateTime(toDate).ToShortDateString();
                        var share_project_analysis = sqlConnection.Query<Shareproject>("getShareProjectAnalysis", new { startDate, toDate },
                        commandType: CommandType.StoredProcedure);

                        return share_project_analysis.ToList();
                    }

                        
                    
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public IEnumerable<LoanApplication> getLoanPerBranchGraphDataAnalysis(string startDate, string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    if (startDate==null || toDate==null || startDate == "" || toDate == "")
                    {
                        var loan_analysis = sqlConnection.Query<LoanApplication>("getLoanAnalysisPerBranch", new { startDate, toDate }, commandType: CommandType.StoredProcedure);

                        return loan_analysis.ToList();
                    }
                    else
                    {
                        startDate = Convert.ToDateTime(startDate).ToShortDateString();
                        toDate = Convert.ToDateTime(toDate).ToShortDateString();
                        var loan_analysis = sqlConnection.Query<LoanApplication>("getLoanAnalysisPerBranch", new { startDate, toDate }, commandType: CommandType.StoredProcedure);

                        return loan_analysis.ToList();
                    }
                    
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public IEnumerable<LoanApplication> getLoanInterestPerBranchGraphDataAnalysis(string startDate, string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    if (startDate == null || toDate == null || startDate == "" || toDate == "")
                    {
                        var loan_analysis = sqlConnection.Query<LoanApplication>("getLoanInterestAnalysisPerBranch", new { startDate, toDate }, commandType: CommandType.StoredProcedure);

                        return loan_analysis.ToList();
                    }
                    else
                    {

                        startDate = Convert.ToDateTime(startDate).ToShortDateString();
                        toDate = Convert.ToDateTime(toDate).ToShortDateString();
                        var loan_analysis = sqlConnection.Query<LoanApplication>("getLoanInterestAnalysisPerBranch", new { startDate, toDate }, commandType: CommandType.StoredProcedure);

                        return loan_analysis.ToList();
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public string index()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LoanApplication> getLoanIssuedVsPaidGraphDataAnalysis(string startDate, string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    if (startDate == null || toDate == null || startDate == "" || toDate == "")
                    {
                        var loan_analysis = sqlConnection.Query<LoanApplication>("getLoanIssuedVsPaid", new { startDate, toDate }, commandType: CommandType.StoredProcedure);

                        return loan_analysis.ToList();
                    }
                    else
                    {

                        startDate = Convert.ToDateTime(startDate).ToShortDateString();
                        toDate = Convert.ToDateTime(toDate).ToShortDateString();
                        var loan_analysis = sqlConnection.Query<LoanApplication>("getLoanIssuedVsPaid", new { startDate, toDate }, commandType: CommandType.StoredProcedure);

                        return loan_analysis.ToList();
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<MembersDetails> getMemberPerBranchGraphDataAnalysis(string startDate, string toDate)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    if (startDate == null || toDate == null||startDate==""||toDate=="")
                    {
                        var data = sqlConnection.Query<MembersDetails>("getMemberCountPerBrnch", new { startDate, toDate },
                           commandType: CommandType.StoredProcedure);


                        return data.ToList();
                    }
                    else
                    {


                        startDate = Convert.ToDateTime(startDate).ToShortDateString();
                        toDate = Convert.ToDateTime(toDate).ToShortDateString();
                        var data = sqlConnection.Query<MembersDetails>("getMemberCountPerBrnch", new { startDate, toDate },
                            commandType: CommandType.StoredProcedure);


                        return data.ToList();
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}