﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface IDeposit
    {
        List<Deposits> list();
        Deposits AddDeposit ( Deposits deposits);
        decimal CheckDepositBalance(string member_number);


    }
}
