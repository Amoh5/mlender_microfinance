﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface IHome
    {
        string index();

        IEnumerable<MembersDetails> getMemberGraphDataAnalysis(string fromDate, string toDate);
        IEnumerable<MembersDetails> getMemberPerBranchGraphDataAnalysis(string fromDate, string toDate);
        IEnumerable<LoanApplication> getLoanGraphDataAnalysis(string fromDate,string toDate);
        IEnumerable<LoanApplication> getLoanPerBranchGraphDataAnalysis(string fromDate, string toDate);
        IEnumerable<LoanApplication> getLoanInterestPerBranchGraphDataAnalysis(string fromDate, string toDate);
        IEnumerable<LoanApplication> getLoanIssuedVsPaidGraphDataAnalysis(string fromDate, string toDate);
        IEnumerable<Shareproject> getShareprojectGraphDataAnalysis(string fromDate, string toDate);


    }
}
