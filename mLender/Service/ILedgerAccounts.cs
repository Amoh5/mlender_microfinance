﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface ILedgerAccounts
    {
         List<Ledger> GetLedgers();
        Ledger AddLedger(bool IsNew, Ledger model);
        Ledger EditLedger(bool IsNew, Ledger model);
        Ledger DeleteLedger(Ledger model);



    }
}
