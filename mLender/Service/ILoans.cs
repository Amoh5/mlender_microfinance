﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface ILoans
    {
        List<LoanApplication> getAppliedLoans();
        List<LoanApplication> getLoanDetails(string loan_no);

        LoanApplication ApplyLoan(LoanApplication application);

        List<ApproveLoan> getLoansToBeApproved(string loan_no);

        ApproveLoan ApproveLoan(string loanno, ApproveLoan approve);
        LoanApplication EditLoan(string loanno, LoanApplication application);
        List<LoanApplication> GetMemberLoans(string member_number, LoanApplication application);
        EffectLoan EffectLoan(string loan_no, EffectLoan application);
        string CheckLoanPolicies(string loan_no);
        string CheckLoanDetails(string loan_no);
        Guarantors AddGuarantor(Guarantors guarantors, string loan_no);
        IEnumerable<Guarantors> GetLoanGuarantors(string loan_no);
        int DeleteGuarantor(string loan_no, string guarantor_number);
        int DeleteCollateral(string id,string loan_no);
        int cancelLoan(string loan_no);
        bool LoanIsSecured(string loan_no);
        bool HasMemberPaidUpfrontFee(string loan_no);
        bool IsMemberHavingSameLoan(string member_no,string loanCode);
        bool HasExceededLoanPeriodAndLoanAmount(string loanCode,decimal loan_period,decimal loan_amount);
    }
}
