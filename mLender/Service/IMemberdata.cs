﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface IMemberdata
    {
        List<MembersDetails> getAllMembers();
        MembersDetails AddMember(string memberno,MembersDetails membersDetails);
        MembersDetails EditMember(string id,MembersDetails membersDetails);
        MembersDetails WithdrawMember(string memberno);
        MembersDetails CheckDeposit(string memberno);
        MembersDetails CheckDeposit(string memberno,DateTime date);
        MemberRefund RefundMember();
        MemberRefund RefundMember(MemberRefund memberRefund);
        MemberPayment LoanPayment(MemberPayment payment);
        bool checkIfUnique(string IDno,string email, string Telephone);






    }
}
