﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface IPayment
    {
        List<MemberPayment> ListPayments();
        MemberPayment AddPayments(MemberPayment memberPayment);



    }
}
