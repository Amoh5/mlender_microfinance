﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface ISacco
    {
        CompanyBranches Add(CompanyBranches model,string branch_id);
        CompanyBranches Edit(string id,CompanyBranches model);
        CompanyBranches Delete(string id,CompanyBranches branches);
    }
}
