﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface IcompanyOfficer
    {
        CompanyOfficer AddOfficer(CompanyOfficer officer, string id);
        CompanyOfficer EditOfficer(CompanyOfficer officer,string id);

        int DeleteOfficer(string id);
        bool HasClients(string id);

    }
}
