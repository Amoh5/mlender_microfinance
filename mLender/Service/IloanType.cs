﻿using mLender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mLender.Service
{
    interface IloanType
    {
        List<LoanType> getLoantypes(List<LoanType> model);

        LoanType AddLoanType(LoanType model, bool IsNew);
        LoanType EditLoanType(LoanType model,bool IsNew);
        LoanType DeleteLoanType(string LoanCode);
        bool CheckLoanCodeIfExists(string LoanCode,bool IsNew);
    }
}
