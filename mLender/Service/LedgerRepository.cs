﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace mLender.Service
{
    public class LedgerRepository : ILedgerAccounts
    {
       
        oErrorLog oErrorLog = new oErrorLog();
        string AuditID = GetCurrentUserService.CurrentUser();
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        public Ledger AddLedger(bool IsNew, Ledger model)
        {
             

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var ledger = new Ledger();

                    ledger.Name = model.Name;
                    ledger.Ledger_group = model.Ledger_group;
                    ledger.Allow_Overdraft = Convert.ToBoolean(model.Allow_Overdraft);
                    ledger.IsBank = Convert.ToBoolean(model.IsBank);
                    ledger.Class = model.Class;
                    ledger.Code =model.Code;
                    var ledger_code = ledger.Code;

            var rowsAffected = sqlConnection.Execute("save_ledger_account",
                        new
                        {

                            ledger.Ledger_group,
                            ledger_code,
                            ledger.Name,
                            ledger.Allow_Overdraft,
                            ledger.IsBank,
                            IsNew
                        }, commandType: CommandType.StoredProcedure);

                    

                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
            return model;
        }

        public Ledger DeleteLedger(Ledger model)
        {
            throw new NotImplementedException();
        }

        public Ledger EditLedger(bool IsNew, Ledger model)
        {
            try
            {
                IsNew = false;
                var updateLedger = AddLedger(IsNew,model);
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
            return model;
        }

        public List<Ledger> GetLedgers()
        {
            throw new NotImplementedException();
        }
    }
}