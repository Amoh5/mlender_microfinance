﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using mLender.Models;

namespace mLender.Service
{
    public class LoanApplicationRepository : ILoans
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        
        oErrorLog oErrorLog = new oErrorLog();
        string AuditID = GetCurrentUserService.CurrentUser();
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

        DepositRepository depositRepository = new DepositRepository();

        bool is_new = true;
        string loannumber = "";
        bool insurable;
        decimal loan_deposit_ratio;
        decimal maximum_amount;
        decimal applied_amount;
        decimal maximum_period;
        decimal AmountGuaranteed;
        decimal registration_fee;
        decimal processing_fee_amount;
        decimal loanBalance;
        decimal paidProcessingFee;
        decimal recovered_amount;
        decimal amount_recovered;
        int minimum_giarantors;
        int membership_period;
        public  string loan_status;
        public  string member_number;
        decimal Paid;
        DateTime appraisal_date;
        public Guarantors AddGuarantor(Guarantors guarantorsModel,string loan_no)
        {
            //throw new NotImplementedException();
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var g= new Guarantors();
                    
                    g.guarantor_number = guarantorsModel.guarantor_number;
                    g.amount_guaranteed = guarantorsModel.amount_guaranteed;
                    g.audit_id = AuditID;
                    //g.date_added = guarantorsModel.date_added;
                    g.date_added = DateTime.Now;

                    

                    var saveGuarantor = sqlConnection.Execute("add_guarantors",
                        new 
                        { 
                            loan_no,
                            g.guarantor_number,
                            g.amount_guaranteed,
                            g.audit_id,
                            g.date_added
                        },commandType:CommandType.StoredProcedure);
                    }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return guarantorsModel;
        }

        public LoanApplication ApplyLoan(LoanApplication application)
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var loan = new LoanApplication();


                    var systemuser = AuditID;

                    loan.application_date = application.application_date;

                    loan.member_number = application.member_number;

                    loan.loan_code = application.loan_code;

                    loan.applied_amount = Convert.ToDecimal(application.applied_amount);

                    loan.audit_id = systemuser;
                    loan.repay_period = Convert.ToDecimal(application.repay_period);

                    loan.purpose = application.purpose;

                    loan.insurer = application.insurer;

                    loan.insured = Convert.ToBoolean(application.insured);

                    loan.basic_salary = Convert.ToDecimal(application.basic_salary);

                    loan.house_allowance = Convert.ToDecimal(application.house_allowance);

                    loan.other_allowance = Convert.ToDecimal(application.other_allowance);

                    loan.other_payment = Convert.ToDecimal(application.other_payment);

                    loan.total_deduction = Convert.ToDecimal(application.total_deduction);

                    var officer_id = HttpContext.Current.Session["agentIDNo"].ToString();

                    if (IsMemberHavingSameLoan(application.member_number, application.loan_code))
                    {
                        throw new ArgumentException("The member has an existing loan of the same loan product ");
                    }
                    else
                    {
                        if (!HasExceededLoanPeriodAndLoanAmount(application.loan_code, application.repay_period, application.applied_amount))
                        {


                            var affectedrows = sqlConnection.Execute("saveloanapplication",
                                    new
                                    {
                                        loan.member_number,
                                        loan.application_date,
                                        loan.loan_code,
                                        loan.applied_amount,
                                        loan.audit_id,
                                        loan.purpose,
                                        loan.repay_period,
                                        loan.insurer,
                                        loan.insured,
                                        loan.basic_salary,
                                        loan.house_allowance,
                                        loan.other_allowance,
                                        loan.other_payment,
                                        loan.total_deduction,
                                        is_new,
                                        loannumber,
                                        officer_id

                                    }, commandType: CommandType.StoredProcedure);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return application;
        }

        public ApproveLoan ApproveLoan(string loan_no,ApproveLoan approve)
        {
        LoanApplicationRepository loanApplicationRepository = new LoanApplicationRepository();
         SqlConnection sqlConnection = new SqlConnection(ConnectionString);

            //throw new NotImplementedException();
            try
            {
                LoanApplicationRepository loanDetails = new LoanApplicationRepository();

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var ap = new ApproveLoan();

                    ap.approved_amount = Convert.ToDecimal(approve.approved_amount);
                    ap.appraisal_date = approve.appraisal_date;
                    ap.approved_period = Convert.ToDecimal(approve.approved_period);
                    ap.deduct_oustanding_loan = Convert.ToBoolean(approve.deduct_oustanding_loan);

                    var audit_id = AuditID;




                    loanDetails.CheckLoanDetails(loan_no);


                    if (loanDetails.loan_status.ToLower() == "Pending".ToLower())
                    {
                        loanApplicationRepository.CheckLoanPolicies(loan_no);
                           

                          if (ap.approved_amount <= loanApplicationRepository.maximum_amount)
                            {
                                if (ap.approved_period <= loanApplicationRepository.maximum_period)
                                {
                                if (HasMemberPaidUpfrontFee(loan_no))
                                {
                                    var insert_rows = sqlConnection.Execute("save_loan_appraisal",
                                    new
                                    {
                                        ap.appraisal_date,
                                        loan_no,
                                        ap.approved_amount,
                                        ap.approved_period,
                                        ap.deduct_oustanding_loan,
                                        audit_id


                                    }, commandType: CommandType.StoredProcedure);

                                }
                                else
                                {
                                    throw new ArgumentException("Member has not paid full transaction fee or Registration Fee");

                                }
                            }
                                else
                                {
                                    throw new ArgumentException("Approved period {" + ap.approved_period + "} " +
                                        "cannot be greater than maximum period {" + loanApplicationRepository.maximum_period + "} for this product");

                                }


                            }
                            else
                            {
                                throw new ArgumentException("Approved amount {" + ap.approved_amount + "} " +
                                    "cannot be more than maximum limit {" + loanApplicationRepository.maximum_amount + "}  for this product");
                            }

                       
                    }


                    else
                    {
                        throw new ArgumentException("This loan is already{ " + loanDetails.loan_status + " }.");

                    }
                }

            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
            return approve;
        }

        public string CheckLoanPolicies(string loan_no)
        {

            //throw new NotImplementedException();
            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();

                    var loan_policies = sqlConnection.Query("getLoanPolicies", new { loan_no }, commandType: CommandType.StoredProcedure).First();

                     insurable = loan_policies.insurable;
                    loan_deposit_ratio = loan_policies.loan_deposit_ratio;
                    maximum_amount = loan_policies.maximum_amount;
                    maximum_period = loan_policies.maximum_period;
                    minimum_giarantors = loan_policies.minimum_giarantors;
                    membership_period = loan_policies.membership_period;
                    return loan_policies.ToString();
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public string CheckLoanDetails(string loan_no)
        {
            try
            {
                sqlConnection = new SqlConnection(ConnectionString);
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var loan_details = sqlConnection.Query("checkLoanDetails", 
                        new { loan_number=loan_no },commandType:CommandType.StoredProcedure).SingleOrDefault();
                    loan_status = loan_details.loan_status;
                    Paid = loan_details.Paid;
                    appraisal_date = loan_details.appraisal_date;
                    member_number = loan_details.member_number;
                    return loan_details.ToString();
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }

        public int DeleteGuarantor(string loan_no, string guarantor_number)
        {
            try
            {
                sqlConnection = new SqlConnection(ConnectionString);

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var delete_guarantor = sqlConnection.Execute("deleteGuarantor", new { loan_no,guarantor_number },commandType:CommandType.StoredProcedure);
                    return delete_guarantor;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public LoanApplication EditLoan(string loan_no, LoanApplication application)
        {
            try
            {
                sqlConnection = new SqlConnection(ConnectionString);

                using (sqlConnection)
                {

                    LoanApplicationRepository loanDetails = new LoanApplicationRepository();

                    var applied_amount =Convert.ToDecimal(application.applied_amount);
                    var application_date = application.application_date;
                    var period = Convert.ToDecimal(application.repay_period);

                    loanDetails.CheckLoanPolicies(loan_no);
                    if (loanDetails.maximum_period>=period)
                    {

                    if (loanDetails.maximum_amount>= applied_amount)
                    {
                        loanDetails.CheckLoanDetails(loan_no);

                        if (loanDetails.loan_status == "Pending")
                        {
                            sqlConnection.Open();
                            var edit_loan = sqlConnection.Execute("[editLoanDetails]",
                                new { loan_number = loan_no, applied_amount, application_date, period }, commandType: CommandType.StoredProcedure);
                        }
                        else
                        {
                            throw new ArgumentException("Only Pending Loans can be edited!");
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Approved amount {" + application.applied_amount + "} " +
                            "cannot be more than maximum limit {" + loanDetails.maximum_amount + "} " +
                            " for this loan type");

                    }
                    }
                    else
                    {
                        throw new ArgumentException("Approved period {" + application.repay_period + "} " +
                                "cannot be greater than maximum period {" + loanDetails.maximum_period + "} for this product");

                    }
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
            return application;
        }

        public EffectLoan EffectLoan( string loan_no,EffectLoan application)
        {
            try
            {
                LoanApplicationRepository loanDetails = new LoanApplicationRepository();

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var effect = new EffectLoan();
                    var audit_id = AuditID;


                    effect.effect_date = application.effect_date;
                    effect.net_amount = application.net_amount;
                    effect.loan_no = application.loan_no;
                    effect.member_number = application.member_number;
                    effect.processing_fee = application.processing_fee;
                    effect.insurance_premium = application.insurance_premium;
                    effect.other_loan_charges = application.other_loan_charges;
                    effect.loan_to_clear = "";
                    effect.voucherNo = "";
                    var description = "";
                    var payment_mode = "";
                    var status = "Examined";
                    effect.credit_account = application.credit_account;

                    loanDetails.CheckLoanDetails(loan_no);
                    if (loanDetails.appraisal_date <= effect.effect_date)
                    {
                        var affected_row = sqlConnection.Execute("save_effect_loan",
                            new
                            {
                                effect.effect_date,
                                effect.loan_no,
                                effect.member_number,
                                effect.net_amount,
                                description,
                                payment_mode,
                                status,
                                effect.processing_fee,
                                effect.insurance_premium,
                                effect.other_loan_charges,
                                effect.loan_to_clear,
                                effect.voucherNo,
                                audit_id,
                                effect.credit_account
                            }, commandType: CommandType.StoredProcedure);

                    }
                    else
                    {
                        throw new ArgumentException("Effect date { " + effect.effect_date.ToShortDateString() + " } " +
                            "cannot be ealier than Appraisal date { " + loanDetails.appraisal_date.ToShortDateString() + " } ");
                    }

                    
                    return effect;

            }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public List<LoanApplication> getAppliedLoans()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Guarantors> GetLoanGuarantors(string loan_no)
        {
            try
            {
                sqlConnection = new SqlConnection(ConnectionString);

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    IEnumerable<Guarantors> loan_guarantors = sqlConnection.Query<Guarantors>("getAllGuarantors_perLoan", 
                        new { loan_no},commandType:CommandType.StoredProcedure).ToList();
                    return loan_guarantors;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public int cancelLoan(string loan_no)
        {
            try
            {
                LoanApplicationRepository loanDetails = new LoanApplicationRepository();

                loanDetails.CheckLoanDetails(loan_no);

                if (loanDetails.Paid<=0)
                {
                    sqlConnection = new SqlConnection(ConnectionString);

                    using (sqlConnection)
                    {
                        sqlConnection.Open();
                        var cancelLoan = sqlConnection.Execute("cancelLoan",
                            new { loan_no }, commandType: CommandType.StoredProcedure);
                        return cancelLoan;
                    }
                }
                else
                {
                    throw new ArgumentException("The member has already repaid the Loan,it cannot be cancelled!");
                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public List<ApproveLoan> getLoansToBeApproved(string loan_no)
        {
            try
            {
                using (sqlConnection)
                {

                    sqlConnection.Open();

                    LoanApplicationRepository loanDetails = new LoanApplicationRepository();

                    loanDetails.CheckLoanDetails(loan_no);

                    if (loanDetails.loan_status.ToLower()=="Pending".ToLower())
                    {
                        var get_loan_to_approve = sqlConnection.Query<ApproveLoan>
                        ("get_member_applied_loans_pending", new { loan_no }, commandType: CommandType.StoredProcedure).ToList();
                        return get_loan_to_approve;

                    }
                    else
                    {
                        throw new ArgumentException("This loan is already{" + loanDetails.loan_status + "}.");
                    }


                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }

        public List<LoanApplication> GetMemberLoans(string member_number, LoanApplication application)
        {
            try
            {
            
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var loanList= sqlConnection.Query<LoanApplication>("getMemberLoans",
                        new { member_number }, commandType: CommandType.StoredProcedure).ToList();

                    return loanList;

                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }

        public bool LoanIsSecured(string loan_no)       {
             


            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var loancollateral = sqlConnection.Query<dynamic>("getLoanCollateralPerLoan",
                        new { loan_number = loan_no }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    AmountGuaranteed=loancollateral.AmountGuaranteed;

                    var appliedAmount= sqlConnection.Query("getLoansPendingApprovaL",
                        new { loan_number = loan_no }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    applied_amount = appliedAmount.applied_amount;

                    if (applied_amount<=AmountGuaranteed)
                    {
                        return true;

                    }
                    else
                    {
                        return false;

                    }


                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }

        public List<LoanApplication> getLoanDetails(string loan_no)
        {
            
                try
                {
                    sqlConnection = new SqlConnection(ConnectionString);

                    using (sqlConnection)
                    {
                        sqlConnection.Open();

                        var details = sqlConnection.Query<LoanApplication>("getLoanDetails",
                            new { loan_no }, commandType: CommandType.StoredProcedure).ToList();
                        
                    return details;
                    }
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }
            }

        public bool HasMemberPaidUpfrontFee(string loan_no)
        {
            try
            {
                 

                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var registrationFeePaid = sqlConnection.Query("getRegistrationFeePaid", new {loan_no },commandType:CommandType.StoredProcedure).SingleOrDefault();

                    var loanProcessingFeePaid = sqlConnection.Query("getProcessingFeePaid", new{loan_no },
                                                                     commandType: CommandType.StoredProcedure).SingleOrDefault();

                    var processingFee= 
                        sqlConnection.Query("getProcessingFeeAmount", new { loan_no}, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    var registrationFee= 
                        sqlConnection.Query("getRegFeeAmount",new { loan_no} , commandType: CommandType.StoredProcedure).SingleOrDefault();


                    processing_fee_amount = processingFee.processing_fee_amount;
                    registration_fee = registrationFee.registration_fee;

                    recovered_amount = loanProcessingFeePaid.recovered_amount;
                    amount_recovered = registrationFeePaid.amount_recovered;


                    var requiredFee = processing_fee_amount + registration_fee;
                    var paidFee = recovered_amount + amount_recovered;

                    decimal value = decimal.Compare(paidFee,requiredFee);

                    if (value>0||value==0)
                    {
                        return true;

                    }
                    else
                    {
                        return false;
                    }


                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                return false;
            }
        }

        public int DeleteCollateral(string id,string loan_no)
        {
            try
            {
                LoanApplicationRepository loanDetails = new LoanApplicationRepository();

                loanDetails.CheckLoanDetails(loan_no);


                if (loanDetails.loan_status.ToLower() == "Pending".ToLower())
                {
                    using (sqlConnection)
                    {
                        sqlConnection.Open();

                        var removed_guarantor = sqlConnection.Execute("delete_collateral", new { @item_code = id }, commandType: CommandType.StoredProcedure);

                        return removed_guarantor;

                    }
                }
                else
                {
                    throw new Exception("Only collaterals for pending loans can be deleted");
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception (ex.Message);

            }
        }

        public bool IsMemberHavingSameLoan(string member_number, string loan_code)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var balance = sqlConnection.Query("checkMemberIfHavingSameLoan", 
                        new { member_number, loan_code },commandType:CommandType.StoredProcedure).SingleOrDefault();

                    loanBalance = balance.Balance;

                    if (loanBalance>0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
            //finally{
            //    sqlConnection.Close();
            //}
        }

        public bool HasExceededLoanPeriodAndLoanAmount(string loan_Code,decimal applied_period, decimal applied_amount)
        {
            try
            {
                sqlConnection = new SqlConnection(ConnectionString);

                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var loanProductDetails = sqlConnection.Query("getLoanProductDetails",
                        new { loan_Code },commandType:CommandType.StoredProcedure).SingleOrDefault();

                    var loanPeriod = loanProductDetails.maximum_period;
                    var maximumAmount = loanProductDetails.maximum_amount;
                    var minimumAmount = loanProductDetails.minimum_amount;

                    decimal periodvalue = decimal.Compare(applied_period, loanPeriod);
                    decimal max_amountvalue = decimal.Compare(applied_amount, maximumAmount);
                    decimal min_amountvalue = decimal.Compare(applied_amount, minimumAmount);


                    if (periodvalue > 0)

                    {
                        throw new ArgumentException("Applied period cannot exceed the set period for this product.");
                    }
                    else if (max_amountvalue > 0)
                    {
                        throw new ArgumentException("Applied amount cannot exceed the set maximum amount for this product.");

                    }
                    else if (min_amountvalue<0)
                    {
                        throw new ArgumentException("Applied amount cannot be less than the set minimum amount for this product.");

                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }
    }
    }
