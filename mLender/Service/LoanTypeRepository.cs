﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace mLender.Service
{
    public class LoanTypeRepository : IloanType
    {
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;

        oErrorLog oErrorLog = new oErrorLog();
        string AuditID = GetCurrentUserService.CurrentUser();
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        private LoanType loantype = new LoanType();

        public LoanType AddLoanType(LoanType model,bool IsNew)
        {
            try
            {
                string ConnectionString =
                ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
         SqlConnection sqlConnection = new SqlConnection(ConnectionString);
                        
                    loantype.code = model.code;
                    loantype.name = model.name;
                    loantype.maximum_amount = Convert.ToDecimal(model.maximum_amount);

                    loantype.minimum_amount = Convert.ToDecimal(model.minimum_amount);

                    loantype.interest = Convert.ToDecimal(model.interest);

                    loantype.repayment_method = model.repayment_method;

                    loantype.loan_deposit_ratio = Convert.ToDecimal(model.loan_deposit_ratio);

                    loantype.deduct_full_interest_first_month = Convert.ToBoolean(model.deduct_full_interest_first_month);

                    loantype.insurable = Convert.ToBoolean(model.insurable);

                    loantype.deduct_premium_on_first_month = Convert.ToBoolean(model.deduct_premium_on_first_month);
                    loantype.ignore_3rd_rule = Convert.ToBoolean(model.ignore_3rd_rule);

                    loantype.maximum_period = Convert.ToInt16(model.maximum_period);

                    loantype.minimum_giarantors = Convert.ToInt16(model.minimum_giarantors);

                    loantype.trustee_loan = Convert.ToBoolean(model.trustee_loan);

                    loantype.membership_period = Convert.ToInt16(model.membership_period);

                    loantype.interest_account = model.interest_account;
                    loantype.principal_account = model.principal_account;
                    loantype.allow_check_off = Convert.ToBoolean(model.allow_check_off);

                    loantype.processing_fee_account = model.processing_fee_account;
                    loantype.processing_fee_amount = model.processing_fee_amount;
                    loantype.is_Processing_fee_percentage = model.is_Processing_fee_percentage;

                    loantype.appraisal_fee_account = model.appraisal_fee_account;
                    loantype.appraisal_fee_amount = model.appraisal_fee_amount;
                    loantype.is_appraisal_fee_percentage = model.is_appraisal_fee_percentage;


                    loantype.penalty_account = model.penalty_account;
                    loantype.penalty_amount = model.penalty_amount;
                    loantype.is_Penalty_Percentage = model.is_Penalty_Percentage;

               
                    //compare minimum and maximum amount
                    int status = loantype.minimum_amount.CompareTo(loantype.maximum_amount);

                if (status < 0)
                {

                    if (!CheckLoanCodeIfExists(loantype.code, IsNew))
                    {

                        ///insert to db 
                        ///
                        using (sqlConnection)
                        {
                            sqlConnection.Open();
                            var affected_rows =
                                sqlConnection.Execute("saveLoanProduct", new
                                {
                                    loantype.code,
                                    loantype.name,
                                    loantype.maximum_amount,
                                    loantype.minimum_amount,
                                    loantype.interest,
                                    loantype.repayment_method,
                                    loantype.deduct_full_interest_first_month,
                                    loantype.loan_deposit_ratio,
                                    loantype.insurable,
                                    loantype.deduct_premium_on_first_month,
                                    loantype.ignore_3rd_rule,
                                    loantype.maximum_period,
                                    loantype.minimum_giarantors,
                                    loantype.trustee_loan,
                                    loantype.membership_period,
                                    loantype.interest_account,
                                    loantype.principal_account,
                                    loantype.allow_check_off,

                                    loantype.processing_fee_account,
                                    loantype.processing_fee_amount,
                                    loantype.is_Processing_fee_percentage,

                                    loantype.appraisal_fee_account,
                                    loantype.appraisal_fee_amount,
                                    loantype.is_appraisal_fee_percentage,


                                    loantype.penalty_account,
                                    loantype.penalty_amount,
                                    loantype.is_Penalty_Percentage

                                }, commandType: CommandType.StoredProcedure);
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Loan Code already exists! Try another one.");
                    }
                }
                else
                {
                    throw new ArgumentException("Minimum amount cannot be greater than maximum amount.");

                }                                              
                

            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
            return model;
        }

        public bool CheckLoanCodeIfExists(string LoanCode,bool IsNew)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var code = sqlConnection.Query("checkIfLoanCodeExists",
                        new { LoanCode }, commandType: CommandType.StoredProcedure).Count();

                    if (code>0&& IsNew)
                    {
                        return true;

                    }
                    else
                    {
                        return false;

                    }

                }

            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public LoanType DeleteLoanType(string LoanCode)
        {
            throw new NotImplementedException();
        }

        public LoanType EditLoanType(LoanType model,bool IsNew)
        {
            try
            {
                return AddLoanType(model,IsNew);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }

        public List<LoanType> getLoantypes(List<LoanType> model )
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var list=sqlConnection.Query<LoanType>
                        ("get_loan_products", null, commandType: CommandType.StoredProcedure).ToList();
                    return list;

                }
            }
            catch (Exception ex)
            {

                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
            return model;


        }
    }
}