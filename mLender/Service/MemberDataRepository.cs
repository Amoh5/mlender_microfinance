﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Dapper;
using mLender.Models;

namespace mLender.Service
{
    public class MemberDataRepository : IMemberdata
    {
        public bool IsNew;
        public string memberno;
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        string AuditID = GetCurrentUserService.CurrentUser();




        public MembersDetails AddMember(string id, MembersDetails membersDetails)
        {
            try
            {


                using (sqlConnection)
                {
                    sqlConnection.Open();
                    string Filedirectory = System.Web.Hosting.HostingEnvironment.MapPath("~/ImageAndPdfFiles");

                    var md = new MembersDetails();

                    md.first_name = membersDetails.first_name;
                    md.other_name = membersDetails.other_name;
                    md.gender = membersDetails.gender;
                    md.date_of_birth =membersDetails.date_of_birth;
                    md.date_of_registration = membersDetails.date_of_registration;
                    md.residence = membersDetails.residence;
                    md.occupation = membersDetails.occupation;
                    md.branch_id = membersDetails.branch_id;
                    md.idno = membersDetails.idno;
                    md.telephone = membersDetails.telephone;
                    md.email_address = membersDetails.email_address;
                    md.marital_status = membersDetails.marital_status;
                    //md.photo = md.photo;
                    md.payroll_no = membersDetails.payroll_no;
                    md.Classification = membersDetails.Classification;
                    md.IsStaff = membersDetails.IsStaff;
                    memberno = membersDetails.member_number;
                    //md.officer_id = (string)Session["agentIDNo"];
                    md.officer_id = HttpContext.Current.Session["agentIDNo"].ToString();
                    md.Referee = membersDetails.Referee;


                    HttpPostedFile postedFile = HttpContext.Current.Request.Files["photo"];

                    var photoName = Path.GetFileName(postedFile.FileName);

                    if (postedFile != null && postedFile.ContentLength> 0)
                    {
                        postedFile.SaveAs(Path.Combine(Filedirectory, photoName));
                    }
                    md.photo =photoName;

                    if (checkIfUnique(md.idno, md.email_address, md.telephone))
                    {


                        var save_members_details = sqlConnection.Execute("savememberdetails", new
                        {


                            md.first_name,
                            md.other_name,
                            md.gender,
                            md.date_of_birth,
                            md.date_of_registration,
                            md.residence,
                            md.occupation,
                            md.idno,
                            md.telephone,
                            md.email_address,
                            md.marital_status,
                            md.photo,
                            md.payroll_no,
                            md.Classification,
                            md.IsStaff,
                            memberno,
                            md.officer_id,
                            md.Referee
                        }, commandType: CommandType.StoredProcedure);
                    }
                    else
                    {
                        throw new ArgumentException("IDNo,Telephone and Email exist in the records.");
                    }
            }

            }
            catch (Exception ex)
            {
                oErrorLog oErrorLog = new oErrorLog();

                oErrorLog.WriteErrorLog(ex.Message);

                throw  new Exception(ex.Message);
            }
            return membersDetails;
        }

        public MembersDetails CheckDeposit(string memberno)
        {
            throw new NotImplementedException();
        }

        public MembersDetails CheckDeposit(string memberno, DateTime date)
        {
            throw new NotImplementedException();
        }

        public List<MembersDetails> getAllMembers()
        {
            throw new NotImplementedException();
        }

        public MemberPayment LoanPayment(MemberPayment payment)
        {
            //throw new NotImplementedException();
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var member = new MemberPayment();
                    member.member_number = payment.member_number;
                    member.payment_date = payment.payment_date;
                    member.voucher_no = payment.voucher_no;
                    member.reference_no = payment.reference_no;
                    member.payment_mode = "Mpesa";
                    member.bank_account = payment.bank_account;
                    member.amount = payment.amount;

                    var audit_id = AuditID;



                    var insert_payment =
                        sqlConnection.Execute("saveMemberPayment",
                        new
                        {
                            member.member_number,
                            member.bank_account,
                            member.payment_date,
                            member.amount,
                            member.voucher_no,
                            member.reference_no,
                            member.payment_mode,
                            audit_id



                        }, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                oErrorLog oErrorLog = new oErrorLog();

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
            return payment;
        }

        public MemberRefund RefundMember(MemberRefund model)
        {
            //throw new NotImplementedException();
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();
                    var refunds = new MemberRefund();

                    refunds.RefundDate = model.RefundDate;
                    refunds.MemberNumber = model.MemberNumber;
                    refunds.RefundType = model.RefundType;
                    refunds.RefundNumber = model.RefundNumber;
                    refunds.ChargeAccount = model.ChargeAccount;
                    refunds.Narration = model.Narration;
                    refunds.ReferenceNo = model.ReferenceNo;
                    refunds.Amount = model.Amount;
                    //var AuditID = HttpContext.User.Identity.GetUserName();
                     AuditID = GetCurrentUserService.CurrentUser();



                    var insertRefund = sqlConnection.Execute("saveMemberRefunds",
                                        new
                                        {
                                            refunds.RefundDate,
                                            refunds.MemberNumber,
                                            refunds.RefundType,
                                            refunds.RefundNumber,
                                            refunds.Amount,
                                            refunds.Narration,
                                            refunds.ReferenceNo,
                                            refunds.ChargeAccount,
                                            AuditID
                                        },
                                        commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                oErrorLog oErrorLog = new oErrorLog();

                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
            return model;
        }

        public MemberRefund RefundMember()
        {
            throw new NotImplementedException();
        }

        public MembersDetails WithdrawMember(string memberno)
        {
            throw new NotImplementedException();
        }

        public MembersDetails EditMember(string id ,MembersDetails membersDetails)
        {
            try
            {
                
                    IsNew = false;
                    memberno = id;
                    MemberDataRepository memberData = new MemberDataRepository();

                    return memberData.AddMember(id,membersDetails);

                
            }
            catch (Exception ex)
            {
                oErrorLog oErrorLog = new oErrorLog();


                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public bool checkIfUnique(string IDno, string email, string Telephone)
        {
            try
            {
                using (sqlConnection)
                {

                    var memberData = sqlConnection.Query("checkUniqueMemberData", //check count if greater than zero then not unique
                        new {IDno,email,Telephone },commandType:CommandType.StoredProcedure).SingleOrDefault();

                    //we use int since we arechecking their counts
                    int id = memberData.IDNo;
                    int mail = memberData.Email;
                    int MobileNo = memberData.Telephone;

                    if (id>0) 
                    {
                        throw new ArgumentException("Similar IDNo exists in database");
                    }
                    else if (mail>0)
                    {
                        throw new ArgumentException("Similar email address exists in database");

                    }
                    else if (MobileNo>0)
                    {
                        throw new ArgumentException("Similar email address exists in database");

                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                oErrorLog oErrorLog = new oErrorLog();


                oErrorLog.WriteErrorLog(ex.Message);
                throw new Exception(ex.Message);
            }
            finally
            {
                sqlConnection.Dispose();
                sqlConnection.Close();
            }
        }
    }
}