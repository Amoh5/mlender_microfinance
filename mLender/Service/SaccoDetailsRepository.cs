﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using mLender.Models;

namespace mLender.Service
{
    public class SaccoDetailsRepository : ISacco
    {
        oErrorLog oErrorLog = new oErrorLog();
        private static string ConnectionString =
            ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;

        private SqlConnection sqlConnection = new SqlConnection(ConnectionString);
        string AuditID = GetCurrentUserService.CurrentUser();
        int number;
        public CompanyBranches Add(CompanyBranches model,string branch_id)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    var company = new CompanyBranches();
                    company.branch_name = model.branch_name;

                    company.officer_name = model.officer_name;

                    company.location = model.location;

                    company.telephone = model.telephone;

                    company.email_address = model.email_address;

                    bool allow_check_off =model.allow_check_off;

                    company.control_account = model.control_account;

                    company.minimum_contribution = model.minimum_contribution;

                    company.minimum_shares =model.minimum_shares;

                    company.registration_fee =model.registration_fee;

                    company.rejoining_fee = model.rejoining_fee;

                    company.check_off_date =model.check_off_date;

                    company.branch_id = model.branch_id;


                    var affectedRows =
                            sqlConnection.Execute("save_branch",
                            new
                            {
                                company.branch_id,
                                company.branch_name,
                                company.officer_name,
                                company.location,
                                company.telephone,
                                company.email_address,
                                allow_check_off,
                                company.control_account,
                                company.minimum_contribution,
                                company.minimum_shares,
                                company.registration_fee,
                                company.rejoining_fee,
                                company.check_off_date

                            }, commandType: CommandType.StoredProcedure);


                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }

            return model;
        }

        public CompanyBranches Delete(string branch_id, CompanyBranches company)
        {
            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    if (branch_id != null)
                    {

                    //check if members for this company exist
                     number = sqlConnection.Query("checkMembersPerCompany", new { branch_id }, commandType: CommandType.StoredProcedure).Count();
                    if (number==0)
                    {
                        var delete_institution = sqlConnection.Execute
                            ("deleteCompany",new{ branch_id },commandType:CommandType.StoredProcedure);
                    }
                    else
                    {
                        throw new ArgumentException("This company has members");
                    }
                    }
                    else
                    {
                        throw new ArgumentException("Company with id { "+ branch_id + " } does not exist");

                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return company;
        }

        public CompanyBranches Edit(string id, CompanyBranches model)
        {
            try
            {
                return Add(model, id);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);

                throw new Exception(ex.Message);
            }
        }
    }
}