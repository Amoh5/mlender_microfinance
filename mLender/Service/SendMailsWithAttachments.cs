﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace mLender.Service
{
    public class SendMailsWithAttachments
    {
        private static string ConnectionString =
                ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;


        public string BulkEmail(string email)
        {
            BuildEmail(email);
            return "Success";
        }

        public static void BuildEmail(string email)
        {
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);
            ArrayList list_emails = new ArrayList();  // In this arraylist you can set the group of mails that you want to communicate  
            list_emails.Add(email);

            using (sqlConnection)
            {
                sqlConnection.Open();
                var emailSettings = sqlConnection.Query("SELECT * FROM EmailSettings", commandType: CommandType.Text).SingleOrDefault();
                var host = emailSettings.smtpserver;
                var smtpport = emailSettings.smtpport;
                var user = emailSettings.smtpuser;
                var password = emailSettings.smtppassword;
                var ssl = emailSettings.SSL;
                var fromAddress = emailSettings.FromAddress;
                var subject = emailSettings.Subject;
                var Body = emailSettings.Body;
                foreach (string email_to in list_emails)
                {

                    string[] filepathhs = Directory.GetFiles(@"E:\Pets\C#\Sacco\ASP.NET\mLender\mLender\LoanStatements\", "*pdf");
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress(fromAddress);   // Here you need to declare the from mail address  
                    message.To.Add(new MailAddress(email_to));  // here in general you need to declare the recepient mail, but here it will assigning from array list declared above  
                    message.Subject = subject;    // Here you can declare the subject content  
                    message.Body = Body;      // Here you can add your specific message   


                    foreach (var filepath in filepathhs)
                    {
                        var attachment = new Attachment(filepath);   // here you can attach a file as a mail attachment  
                        message.Attachments.Add(attachment);

                    }
                    SendEmail(message);    // This invokes the mail connection properties method  
                }
            }
        }

        public static void SendEmail(MailMessage message)
        {
             SqlConnection sqlConnection = new SqlConnection(ConnectionString);

             
            try
            {
                using (message)
                {

                    using (sqlConnection)
                    {
                        sqlConnection.Open();
                        var emailSettings = sqlConnection.Query("SELECT * FROM EmailSettings", commandType: CommandType.Text).SingleOrDefault();
                        var host = emailSettings.smtpserver;
                        var smtpport = emailSettings.smtpport;
                        var user = emailSettings.smtpuser;
                        var password = emailSettings.smtppassword;
                        var ssl = emailSettings.SSL;
                        var fromAddress = emailSettings.FromAddress;
                        var subject = emailSettings.Subject;
                        var Body = emailSettings.Subject;
                        SmtpClient client = new SmtpClient();
                        client.Host = host;     // These are the host connection properties  
                        client.Port = Convert.ToInt32(smtpport);
                        client.EnableSsl = Convert.ToBoolean(ssl);
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new System.Net.NetworkCredential(user, password);    // here you need to declare the credentials of the sender 


                        client.Send(message);      // And finally this is the line which executes our process and sends mail   
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}