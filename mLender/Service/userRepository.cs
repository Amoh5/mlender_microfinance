﻿using Dapper;
using mLender.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace mLender.Service
{
    public class userRepository : Iusers
    {
		private static string ConnectionString =
				ConfigurationManager.ConnectionStrings["JIOKOE"].ConnectionString;
		private SqlConnection sqlConnection = new SqlConnection(ConnectionString);

		oErrorLog oErrorLog = new oErrorLog();
		public string getBranch(string usermail)
        {
			try
			{
				using (sqlConnection)
				{
					sqlConnection.Open();
					var branch = sqlConnection.Query("getBranchPerOfficer",
						new { email_address = usermail }, commandType: CommandType.StoredProcedure).SingleOrDefault();
					return branch.branch_name;
				}
			}
			catch (Exception ex)
			{

				throw new Exception(ex.Message);
			}
        }
    }
}