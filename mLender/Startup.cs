﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using mLender.Models;

[assembly: OwinStartupAttribute(typeof(mLender.Startup))]
namespace mLender
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            JiokoeDbContext jiokoeDb = new JiokoeDbContext();
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(jiokoeDb));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(jiokoeDb));
            //create first default admin role and admin user
            if (!RoleManager.RoleExists("Admin"))
            {
                //First create admin bool

                var role = new IdentityRole()
                {
                    Name = "Admin"
                };
                RoleManager.Create(role);

                //create admin superUser who will assign roles to developers
                var user = new ApplicationUser()
                {
                    UserName = "amoskirui5@gmail.com"
                };
                string userPWD = "#Amoskirui5";
                var createUser = UserManager.Create(user, userPWD);

                //add  default user to admin role
                if (createUser.Succeeded)
                {
                    var addToAdminRole = UserManager.AddToRole(user.Id, "Admin");
                }

            }
            // creating Creating Manager role    
            if (!RoleManager.RoleExists("Manager"))
            {
                var role = new IdentityRole()
                {
                    Name = "Manager"
                };
                RoleManager.Create(role);

            }

            // creating Creating Employee role    
            if (!RoleManager.RoleExists("Accountant"))
            {
                var role = new IdentityRole()
                {
                    Name = "Accountant"
                };
                RoleManager.Create(role);

            }
        }
    }

}
